import java.io.Serializable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

/**
 * BackOrders maintains a collection of Washers that have backorders that need
 * to be processed whenever more washers are added to the inventory. The data
 * structure used is a TreeMap, which implements the Map interface. The washers
 * serve as keys in the map, and the value of each washer (key) is a Queue of
 * BackOrders.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class BackOrders implements Serializable {
	private static final long serialVersionUID = 1L;
	private Map<Washer, Queue<BackOrder>> delayedOrders;

	/**
	 * Constructor for BackOrders
	 * 
	 * delayedOrders is instantiated as a TreeMap.
	 */
	public BackOrders() {
		delayedOrders = new TreeMap<>();
	}

	/**
	 * addBackOrder(order: BackOrder): void
	 * 
	 * Adds a BackOrder object to the Queue value associated with the key. The key
	 * is the Washer object which is kept by the back-order. The Queue's offer
	 * method is used to add a back-order.
	 * 
	 * @param order - the backorder to be offered into the queue
	 */
	public void addBackOrder(BackOrder order) {
		Washer key = order.getWasher();
		if (delayedOrders.containsKey(key)) {
			delayedOrders.get(order.washer).offer(order);
		} else {
			// initially this collection is empty
			// keys and values are created one at a time as each washer is encountered.
			Queue<BackOrder> value = new LinkedList<>();
			value.offer(order);
			delayedOrders.put(key, value);
		}
	}

	/**
	 * updateBackOrder(washer: Washer, quantity: int): int
	 * 
	 * Checks the queue of backorders for the washer object passed in and attempts
	 * to complete and remove the backorders from the queue. Backorders are only
	 * removed from queue if there is enough washers passed in as quantity to
	 * satisfy the order. Backorders can be partially processed and kept in the
	 * front of the queue.
	 * 
	 * @param washer   - the washer whose backorders we are to check for
	 * @param quantity - the amount updated in inventory
	 * @return the quantity used up from backorders
	 */
	public int updateBackOrders(Washer washer, int quantity) {
		int quantityUsed = 0;
		int quantityRemained = quantity;
		Queue<BackOrder> orders = delayedOrders.get(washer);
		// return 0 if the washer contains no orders, the value of orders would be null
		if (!delayedOrders.containsKey(washer)) {
			return 0;
		}
		while (quantityRemained > 0 && !orders.isEmpty()) {
			// orders = delayedOrders.get(washer);
			BackOrder orderToUpdate = orders.peek();
			if (quantityRemained >= orderToUpdate.getQuantity()) {
				quantityUsed += orders.remove().getQuantity();
				quantityRemained = quantity - quantityUsed;
			} else {
				orderToUpdate.setQuantity(orderToUpdate.getQuantity() - quantityRemained);
				quantityUsed += quantityRemained;
				quantityRemained = 0;
			}
		}
		return quantityUsed;
	}

}
