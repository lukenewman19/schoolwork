import java.io.Serializable;

/**
 * Customer represents a customer that are WasherCompany will remember. The name
 * and phone number are stored as well as an immutable id. The id is generated
 * by a static counter. Since static fields can't be serialized, we update the
 * static field in Customer when loading from disk by getting the largest id in
 * Customers. equals() and compareTo() are implemented so that a Set may be used
 * as a collection.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Customer implements Comparable<Customer>, Serializable {
	private static final long serialVersionUID = 1L;
	private final int id;
	private String name;
	private String phoneNumber;
	private static int count;

	/**
	 * Constructor for Customer.
	 * 
	 * name and phoneNumber assigned, count is incremented and the id is assigned to
	 * count
	 * 
	 * @param name        - the name of the customer
	 * @param phoneNumber - the phone number of customer
	 */
	public Customer(String name, String phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
		count++;
		id = count;
	}

	/**
	 * getId(): int
	 * 
	 * @return the id of customer
	 */
	public int getId() {
		return id;
	}

	/**
	 * getCount(): int
	 * 
	 * count is not the sum of all customers in customerSet, it may be equal to it
	 * but it is independent of the total number of customers.
	 * 
	 * @return the current count for generating ids
	 */
	public static int getCount() {
		return count;
	}

	/**
	 * setCount(newCount: int): void
	 * 
	 * Sets the count for generating ids. This method is needed because static
	 * fields are not saved during serialization. After deserializing a
	 * WasherCompany from disk the previous static field count is lost, it must be
	 * reset.
	 * 
	 * @param newCount - the new count
	 */
	public static void setCount(int newCount) {
		if (newCount > count) {
			count = newCount;
		}
	}

	/**
	 * getName(): String
	 * 
	 * @return the name of the customer
	 */
	public String getName() {
		return name;
	}

	/**
	 * setName(name: String): void
	 * 
	 * setter method for customers name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * getPhoneNumber(): String
	 * 
	 * @return the phone number of customer
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * setPhoneNumber(): void
	 * 
	 * setter method for customer's phone number
	 * 
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * equals(object: Object): boolean
	 * 
	 * defines equality for customers
	 * 
	 * @param object - the object we are testing for equality
	 * @return true if the object on which this method is invoked equals the object
	 *         passed in
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || getClass() != object.getClass()) {
			return false;
		} else {
			Customer other = (Customer) object;
			return this.compareTo(other) == 0;
		}
	}

	/**
	 * toString(): String
	 * 
	 * @return a description of the customer
	 */
	@Override
	public String toString() {
		return "Customer id: " + getId() + " Name: " + getName() + " Phone Number: " + getPhoneNumber();
	}

	/**
	 * compareTo(other: Customer): int
	 * 
	 * Compares the customer object passed into method with the customer object
	 * which is responsible for invoking the method. Customers are compared via id,
	 * name, and phone number.
	 * 
	 * @param - a reference to another customer object
	 * @return an integer value that makes numerical ordering possible
	 */
	@Override
	public int compareTo(Customer other) {
		if (name.equals(other.name) && phoneNumber.equals(other.phoneNumber)) {
			return 0;
		}
		return id - other.id;
	}
}
