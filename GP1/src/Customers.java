import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

/**
 * Customers maintains a collection of Customer objects in a TreeSet. The idea
 * behind a set is that no two customers may be the same as defined by the
 * compareTo() method of the Customer class.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Customers implements Serializable {
	private static final long serialVersionUID = 1L;
	private Set<Customer> customersSet;

	/**
	 * Constructor for Customers
	 * 
	 * customersSet() is instantiated as a TreeSet().
	 */
	public Customers() {
		customersSet = new TreeSet<>();
	}

	/**
	 * addCustomer(customer: Customer): boolean
	 * 
	 * Adds a customer to the customer set. Customers with the same id as an
	 * existing customer cannot be added.
	 * 
	 * @param customer - the customer to be added
	 * @return true if addition was successful, false otherwise (duplicate id)
	 */
	public boolean addCustomer(Customer customer) {
		return customersSet.add(customer);
	}

	/**
	 * searchById(customerId: int): Customer
	 * 
	 * Searches the set for a customer with the customer id provided by the method's
	 * argument
	 * 
	 * @param customerId - the integer id of the customer
	 * @return a reference to the customer object
	 */
	public Customer searchById(int customerId) {
		for (Customer customer : customersSet) {
			if (customer.getId() == customerId) {
				return customer;
			}
		}
		return null;
	}

	/**
	 * retrieveLastId(): int
	 * 
	 * This method obtains the last customer in the set and returns its id. When
	 * loading from file this method is called to set the static field count in
	 * Customer for id generation. Since we increment id numbers by one for each new
	 * customer, this is the only information we need to add new customers to a
	 * washercompany that has been loaded from a file.
	 * 
	 * @return the last id number in the customer set
	 */
	public int retrieveLastId() {
		try {
			// Java breakpoint on this line, dot to the left of line
			// .last() is not a method of type Set only TreeSet so it must be cast first
			return ((TreeSet<Customer>) customersSet).last().getId();

		} catch (NoSuchElementException ex) {
			// the set is empty return 0
			return 0;
		} catch (Exception ex) {
			// return an invalid id if an unknown exception occurs
			System.out.println(ex.getMessage());
			return -1;
		}
	}

	/**
	 * toString(): String
	 * 
	 * Returns the list of customers, one customer per line
	 */
	@Override
	public String toString() {
		String customersList = "Name\t\tPhone Number\tId\n---------------------------------------\n";
		for (Customer customer : customersSet) {
			customersList += String.format("%-12s\t%-10s\t%d%n", customer.getName(), customer.getPhoneNumber(),
					customer.getId());
		}
		return customersList;
	}
}
