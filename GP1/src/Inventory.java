import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Inventory maintains a list of washer types along with their quantity. The
 * data structure used is a TreeMap which implements the Map interface. Washers
 * are ordered in the structure according to the compareTo() method of the
 * Washer class. The washers serve as keys, and the quantity as the value. In
 * addition to adding, updating, and getting a list, the inventory is also
 * consulted by the WasherCompany to get information related to specific
 * washers.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Inventory implements Serializable {
	private static final long serialVersionUID = 1L;
	private Map<Washer, Integer> washers;

	/**
	 * Constructor for Inventory The collection is instantiated as a TreeMap.
	 */
	public Inventory() {
		washers = new TreeMap<>();
	}

	/**
	 * addModel(washer: Washer): boolean
	 * 
	 * Adds a washer to the inventory. Returns false if washer already exists in the
	 * inventory or if the price is negative.
	 * 
	 * @param washer - the washer to be added to inventory
	 * @return true if washer was able to be added, false if washer is already in
	 *         the inventory
	 * 
	 */
	public boolean addModel(Washer washer) {
		if (washers.containsKey(washer) || washer.getPrice() < 0) {
			return false;
		}
		washers.put(washer, 0);
		return true;
	}

	/**
	 * updateQuantity(washer: Washer, quantity: int): boolean
	 * 
	 * Updates the quantity of a specific washer in the inventory. Negative integers
	 * may be used to decrease the quantity such as after a sale. If the washer is
	 * not found in the collection then the method returns false.
	 * 
	 * @param washer   - the washer whose quantity is to be updated
	 * @param quantity - the amount to be added or subtracted from inventory
	 * @return true if washer is in collection and update was successful, false
	 *         otherwise
	 */
	public boolean updateQuantity(Washer washer, int quantity) {
		if (!washers.containsKey(washer)) {
			return false;
		}

		quantity += washers.get(washer);
		washers.put(washer, quantity);
		return true;
	}

	/**
	 * getQuantity(washer: Washer): int
	 * 
	 * Returns the quantity of washers in inventory of that brand and model.
	 * 
	 * @param washer - the washer whose quantity we want to check
	 * @return the quantity of the washers in our inventory, returns -1 if washer
	 *         was not found
	 */
	public int getQuantity(Washer washer) {
		if (washers.containsKey(washer)) {
			return washers.get(washer);
		} else {
			return -1;
		}
	}

	/**
	 * search(brand: String, model: String): Washer
	 * 
	 * Searches the inventory for a specific washer defined by its brand and model,
	 * and then returns a reference for it. The WasherCompany must obtain a
	 * reference for a specific washer to be able to place it on backorder or
	 * alternatively get its price.
	 * 
	 * @param brand - the brand of the washer
	 * @param model - the model of the washer.
	 * @return a reference to the washer if it was found, null if it was not found
	 */
	public Washer search(String brand, String model) {
		Washer match = new Washer(brand, model, 0.00);
		for (Washer washer : washers.keySet()) {
			if (washer.equals(match)) {
				return washer;
			}
		}
		return null;
	}

	/**
	 * toString(): String
	 * 
	 * @return a description and list of washers present in inventory
	 */
	@Override
	public String toString() {
		String washerList = "Brand\t\tModel\t\tPrice, $     Quantity\n------------------------------------------------------\n";
		for (Washer washer : washers.keySet()) {
			washerList += String.format("%-12s\t%-12s\t%.2f\t\t%d%n", washer.getBrand(), washer.getModel(),
					washer.getPrice(), washers.get(washer));
		}
		return washerList;
	}

}
