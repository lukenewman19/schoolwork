import java.io.File;
import java.util.Scanner;

/**
 * UserInterface serves as a point of access for user to manipulate
 * WasherCompany singleton object. Contains one public method to process
 * commands. Private methods work with user commands. UserInterface is also a
 * singleton.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class UserInterface {
	private static UserInterface userInterface;
	private WasherCompany business;
	private Scanner input = new Scanner(System.in);
	public static final int EXIT = 0;
	public static final int ADD_CUSTOMER = 1;
	public static final int ADD_WASHER = 2;
	public static final int ADD_TO_INVENTORY = 3;
	public static final int PURCHASE = 4;
	public static final int LIST_CUSTOMERS = 5;
	public static final int LIST_WASHERS = 6;
	public static final int DISPLAY_TOTAL_SALES = 7;
	public static final int SAVE = 8;
	public static final int HELP = 9;

	/**
	 * Private constructor for UserInterface Instantiates UserInterface and prompts
	 * the user for a file. WasherCompany singleton is created anew or obtained from
	 * file.
	 */
	private UserInterface() {
		if (promptUser("Load data from disk? (yes or no)").equalsIgnoreCase("yes")) {
			promptForFile();
		} else {
			System.out.println("Not loading from disk. New instance created.");
		}
		business = WasherCompany.instance();
	}

	/**
	 * instance(): UserInterface
	 * 
	 * @return the user interface for washer company
	 */
	public static UserInterface instance() {
		if (userInterface == null) {
			userInterface = new UserInterface();
		}
		return userInterface;
	}

	/**
	 * process(): void
	 * 
	 * The main method that processes user commands. Quits upon exit command.
	 */
	public void process() {
		int command;
		help();
		while ((command = getCommand()) != EXIT) {
			switch (command) {
			case ADD_CUSTOMER:
				addCustomers();
				break;
			case ADD_WASHER:
				addWashers();
				break;
			case ADD_TO_INVENTORY:
				addToInventory();
				break;
			case PURCHASE:
				makePurchase();
				break;
			case LIST_CUSTOMERS:
				listCustomers();
				break;
			case LIST_WASHERS:
				listWashers();
				break;
			case DISPLAY_TOTAL_SALES:
				displayTotalSales();
				break;
			case SAVE:
				save();
				break;
			case HELP:
				help();
				break;
			default:
				System.out.println("Not a valid option. Try Again.");
				break;
			}
		}
	}

	/**
	 * promptForFile(): void
	 * 
	 * Obtains filename from user. If the file exists, the WasherCompnay singleton
	 * is taken from file. Calls static method WasherCompany.load(String filename)
	 * to obtain the WasherCompany on file or if there is already an instance of
	 * WasherCompany it will obtain that one and the file will be ignored. This is
	 * to maintain the singleton property of WasherCompany.
	 */
	private void promptForFile() {
		String filename = promptUser("What is the file name? provided file is \"gp1\"");
		File file;
		do {
			file = new File(filename);
			if (file.exists()) {
				business = (WasherCompany) WasherCompany.load(filename);
			} else {
				filename = promptUser("File cannot be found. Enter another file or skip by hitting enter and"
						+ "\na new WasherCompany will be created.");

			}
		} while (!file.exists() && !filename.equals(""));
	}

	/**
	 * getCommand(): int
	 * 
	 * Returns the command given by the user.
	 * 
	 * @return int - the command given by the user, -1 if not integer format
	 */
	private int getCommand() {
		try {
			System.out.println("Enter a command: (9 for help)");
			int command = input.nextInt();
			return command;
		} catch (Exception ex) {
			// User types in something else besides an integer
			System.out.println("Only integers 0 - 9 are valid options.");
			input.nextLine();
			return -1;
		}
	}

	/**
	 * promptUser(prompt: String): String
	 * 
	 * Returns the information from the user asked for in its parameter
	 * 
	 * @param prompt - what is being requested by the system
	 * 
	 * @return String - information provided by user
	 */
	private String promptUser(String prompt) {
		System.out.println(prompt);
		return input.nextLine();
	}

	/**
	 * addCustomers(): void
	 * 
	 * Adds customers to WasherCompany. User provides information for customers to
	 * be created one at a time until going back to main menu.
	 */
	private void addCustomers() {
		input.nextLine();
		do {
			String name = promptUser("Please enter the name of customer: ");
			String phoneNumber = promptUser("Please enter the phone number of customer: ");
			if (business.addCustomer(name, phoneNumber)) {
				System.out.println("Customer " + name + " created and stored.");
			} else {
				System.out.println("Failed to add customer.");
			}
		} while (promptUser("Do you want to add another customer? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * addWashers(): void
	 * 
	 * Adds washers to WasherCompany. User provides information for washers to be
	 * created one at a time until going back to main menu.
	 */
	private void addWashers() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				double price = Double.parseDouble(promptUser("Please enter the price: "));
				if (business.addModel(brand, model, price)) {
					System.out.println("Washer added.");
				} else {
					System.out.println("Unable to add washer.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid value for price. Washer not added.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		} while (promptUser("Do you want to add another washer? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * addToInventory(): void
	 * 
	 * Adds stock to inventory of WasherCompany. Takes in information provided by
	 * user to increase the quantity of a specific washer. User can continue to add
	 * stock to washers until ready for another operation. As written new washers
	 * will not be created if user provided washer brand and model names are not in
	 * inventory.
	 */
	private void addToInventory() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				int quantity = Integer.parseInt(promptUser("Please enter the quantity: "));
				int backOrdersFulfilled = business.addToInventory(brand, model, quantity);
				if (backOrdersFulfilled > 0) {
					System.out.println("Stock updated.  " + backOrdersFulfilled + " backorders able to be completed.");
				} else if (backOrdersFulfilled == 0) {
					System.out.println("Stock updated.");
				} else {
					System.out.println("Unable to add stock. Invalid input, no match for washer or invalid quantity.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage() + ex.toString() + ex.getStackTrace());
			}
		} while (promptUser("Do you want to add stock to another washer? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * makePurchase(): void
	 * 
	 * Attempts to purchase washers for customers. If there aren't enough washers in
	 * inventory for a purchase, a full or partial BackOrder is created. Any partial
	 * amount of washers that can be sold are sold and the rest is back-ordered.
	 */
	private void makePurchase() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				int customerId = Integer.parseInt(promptUser("Please enter the customer ID: "));
				int quantity = Integer.parseInt(promptUser("Please enter the quantity: "));
				// sold is what we were able to sell
				// business.purchase(...) performs the WasherCompany purchase operation
				int sold = business.purchase(brand, model, customerId, quantity);
				if (sold >= 0) {
					if (sold == 0) {
						System.out.println("Out of stock. Placed on backorder.");
					} else if (sold < quantity) {
						System.out.println(sold + " able to be purchased.  The remaining placed on backorder.");
					} else {
						System.out.println("Purchase successful.");
					}
				} else {
					System.out.println("Invalid input.  Unable to process transaction.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		} while (promptUser("Do you want to make another purchase? yes or no").equalsIgnoreCase("yes"));

	}

	/**
	 * listCustomers(): void
	 * 
	 * Calls listCustomers() method of WasherCompany to obtain the list of
	 * customers.
	 */
	private void listCustomers() {
		System.out.println(business.listCustomers());
	}

	/**
	 * listWashers: void
	 * 
	 * Calls listWashers() method of WasherCompany to obtain the list of washers.
	 */
	private void listWashers() {
		System.out.println(business.listWashers());
	}

	/**
	 * displayTotalSales(): void
	 * 
	 * Calls the displayTotalSales() method of WasherCompany to get TotalSales.
	 */
	private void displayTotalSales() {
		System.out.println(business.getTotalSales());
	}

	/**
	 * save(): void
	 * 
	 * Receives the filename from the user and then attempts to save that file by
	 * calling the save() method of WasherCompany. WasherCompany handles its own
	 * saving for now.
	 */
	private void save() {
		input.nextLine();
		String filename = promptUser("Please enter the filename you want to save to: ");
		if (business.save(filename)) {
			System.out.println("Saved Successfully.");
		} else {
			System.out.println("There was a problem in saving this file.");
		}
	}

	/**
	 * help(): void
	 * 
	 * Prints the command values.
	 */
	private void help() {
		System.out.println("0 - Exit\n1 - Add Customer\n2 - Add Washer\n"
				+ "3 - Add to Inventory\n4 - Purchase\n5 - List Customers\n"
				+ "6 - List Washers\n7 - Display Total Sales\n8 - Save\n9 - Help");
	}

	/**
	 * main method instantiates a UserInterface and calls its process() method.
	 */
	public static void main(String[] args) {

		UserInterface.instance().process();

	}
}
