import java.io.Serializable;

/**
 * Washer represents a washer in the WasherCompany's inventory. The brand,
 * model, and price are stored. equals() and compareTo() are used so that a Map
 * may be used for the collections Inventory and BackOrders.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Washer implements Comparable<Washer>, Serializable {
	private static final long serialVersionUID = 1L;
	private final String brand;
	private final String model;
	private final double price;

	/**
	 * Constructor for Washer.
	 * 
	 * Assigns values to all three instance fields.
	 * 
	 * @param brand - the brand name of washer
	 * @param model - the model name of washer
	 * @param price - the price of washer
	 */
	public Washer(String brand, String model, double price) {
		this.brand = brand;
		this.model = model;
		this.price = price;
	}

	/**
	 * getBrand(): String
	 * 
	 * @return the brand name of washer
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * getPrice(): double
	 * 
	 * @return the price of the washer
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * getModel(): String
	 * 
	 * @return the model name of washer
	 */
	public String getModel() {
		return model;
	}

	/**
	 * equals(object: Object): boolean
	 * 
	 * defines equality for Washer objects
	 * 
	 * @param object - the object we are testing for equality
	 * @return true if the two objects are equal as defined by this method's
	 *         implementation; false otherwise
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || getClass() != object.getClass()) {
			return false;
		} else {
			Washer other = (Washer) object;
			return this.compareTo(other) == 0;
		}
	}

	/**
	 * toString(): String
	 * 
	 * @return a description of the washer
	 */
	@Override
	public String toString() {
		return "Brand: " + getBrand() + "\tModel: " + getModel() + "\tPrice: " + getPrice();
	}

	/**
	 * compareTo(other: Washer): int
	 * 
	 * This method compares two washers and returns an integer value that provides a
	 * way of ordering the washers in a collection. They are compared by their
	 * String values of brand and model.
	 * 
	 * @param other - a reference to the other Washer object we are comparing
	 * @return an integer value representing the difference
	 */
	@Override
	public int compareTo(Washer other) {
		if (!brand.equals(other.brand)) {
			return brand.compareTo(other.brand);
		} else {
			return model.compareTo(other.model);
		}
	}
}
