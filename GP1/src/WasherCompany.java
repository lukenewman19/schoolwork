import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * WasherCompany is a singleton class and is serializable. Maintains 3
 * collections of customers, washers, and backorders, and a totalSales field
 * which is the revenue. All public methods, save the instance() method, relate
 * to a specific use case including loading a file from disk.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class WasherCompany implements Serializable {
	private static final long serialVersionUID = 1L;
	private static WasherCompany company;
	private Customers customers;
	private Inventory inventory;
	private BackOrders backOrders;
	private double totalSales;

	/**
	 * Private constructor for WasherCompany singleton. Only called from within the
	 * instance() method.
	 */
	private WasherCompany() {
		customers = new Customers();
		inventory = new Inventory();
		backOrders = new BackOrders();
	}

	/**
	 * instance(): WasherCompany
	 * 
	 * @return - the only WasherCompany instance() allowed to be created or loaded
	 *         from a file by this class.
	 */
	public static WasherCompany instance() {
		if (company == null) {
			company = new WasherCompany();
		}
		return company;
	}

	/**
	 * addCustomer(name: String, phoneNumber: String): boolean
	 * 
	 * Attempts to add a customer to our customer collection.
	 * 
	 * @param name        - the name of customer
	 * @param phoneNumber - the phone number of customer
	 * @return true if customer was able to be created, false otherwise (id already
	 *         exists)
	 */
	public boolean addCustomer(String name, String phoneNumber) {
		return customers.addCustomer(new Customer(name, phoneNumber));
	}

	/**
	 * addModel(brand: String, model: String, price: double): boolean
	 * 
	 * Attempts to add a washer to the company.
	 * 
	 * @param brand - the brand name of washer
	 * @param model - the model name of washer
	 * @param price - the price of washer
	 * @return true if washer was added successfully, false if price provided was
	 *         negative
	 * 
	 */
	public boolean addModel(String brand, String model, double price) {
		return inventory.addModel(new Washer(brand, model, price));
	}

	/**
	 * addToInventory(brand: String, model: String, quantity: int): int
	 * 
	 * Attempts to add the quantity of a specific washer in our inventory. Updates
	 * totalSales if their were back-orders and then again will update inventory.
	 * 
	 * @param brand    - the brand name of washer
	 * @param model    - the model name of washer
	 * @param quantity - the amount added to stock
	 * @return the number of backorders completed
	 * 
	 */
	public int addToInventory(String brand, String model, int quantity) {
		Washer washer = inventory.search(brand, model);
		if (washer != null && quantity > 0) {
			int quantityUsed = backOrders.updateBackOrders(washer, quantity);
			totalSales += quantityUsed * washer.getPrice();
			inventory.updateQuantity(washer, quantity - quantityUsed);
			return quantityUsed;
		} else {
			return -1;
		}
	}

	/**
	 * purchase(brand: String, model: String, customerId: int, quantity: int): int
	 * 
	 * Attempts to purchase washers for specific customers. Four possible outcomes
	 * of this method. There is enough in stock to satisfy a complete order. The
	 * order can only be completed partially due to not having enough in stock, and
	 * the remaining are placed on backorder. We are out of stock and the entire
	 * order is placed on backorder, or the washer or customer id provided does not
	 * match information in our system.
	 * 
	 * @param brand      - the brand name of the washer
	 * @param model      - the model name of the washer
	 * @param customerId - the customer id
	 * @param quantity   - the amount wanted
	 * @return the quantity sold, returns -1 if washer or customer was not found
	 */
	public int purchase(String brand, String model, int customerId, int quantity) {
		if (quantity <= 0) {
			// invalid quantity
			return -1;
		}
		Washer washer = inventory.search(brand, model);
		Customer customer = customers.searchById(customerId);
		if (washer != null && customer != null) {
			int quantityInStock = inventory.getQuantity(washer);
			int quantitySold;
			// if quantity desired is less than or equal to stock on hand
			if (quantity <= quantityInStock) {
				inventory.updateQuantity(washer, -quantity);
				quantitySold = quantity;
			} else {
				// not enough in stock, we will sell what is left and place the rest on
				// back-order
				int quantityBackOrdered = quantity - quantityInStock;
				quantitySold = quantityInStock;
				inventory.updateQuantity(washer, -quantityInStock);
				backOrders.addBackOrder(new BackOrder(customer, washer, quantityBackOrdered));
			}
			totalSales += quantitySold * washer.getPrice();
			return quantitySold;
		} else {
			// Return an invalid int if customer or washer were not found
			return -1;
		}
	}

	/**
	 * listCustomers(): String
	 * 
	 * @return customer list in String format
	 */
	public String listCustomers() {
		return customers.toString();
	}

	/**
	 * listWashers(): String
	 * 
	 * @return the washer list in String format
	 */
	public String listWashers() {
		return inventory.toString();
	}

	/**
	 * getTotalSales(): String
	 * 
	 * @return the totalSales instance field
	 */
	public String getTotalSales() {
		return String.format("Total Sales: $%.2f%n", totalSales);
	}

	/**
	 * save(filename: String): boolean
	 * 
	 * Will save the company object to the file with the name specified in its
	 * parameter.
	 * 
	 * @param filename - the name of the file
	 * @return true if data was successfully saved, false if not
	 */
	public boolean save(String filename) {
		try (ObjectOutputStream data = new ObjectOutputStream(new FileOutputStream(filename))) {
			data.writeObject(company);
			return true;
		} catch (FileNotFoundException ex) {
			System.out.println("The file could not be created.\n" + ex.getMessage());
			return false;
		} catch (InvalidClassException ex) {
			System.out.println("Something is wrong with a class used by serialization.\n" + ex.getMessage());
			return false;
		} catch (IOException ex) {
			System.out.println("An I/O error has occurred.\n" + ex.getMessage());
			return false;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return false;
		}
	}

	/**
	 * load(fileName: String): Object
	 * 
	 * Given a filename this method will open the file and retrieve the object
	 * stored on it. If a WasherCompany object already exists in memory then it will
	 * ignore the file and inform the user. Any errors/exceptions are also printed.
	 * 
	 * @param filename - the name of the file that contains the object
	 * @return returns the WasherCompany singleton whether obtained from disk or the
	 *         one already in use.
	 */
	public static Object load(String filename) {
		try (ObjectInputStream data = new ObjectInputStream(new FileInputStream(filename))) {
			if (company == null) {
				company = (WasherCompany) data.readObject();
				if (company.customers.retrieveLastId() > 0) {
					Customer.setCount(company.customers.retrieveLastId());
				}
			} else {
				System.out.println("You cannot load from file.  Company already exists.");
			}
			return company;
		} catch (FileNotFoundException exception) {
			System.out.println("The file could not be created.\n" + exception.getMessage());
			return null;
		} catch (ClassNotFoundException exception) {
			System.out.println("Something is wrong with a class used by serialization.\n" + exception.getMessage());
			return null;
		} catch (IOException exception) {
			System.out.println("An I/O error has occurred.\n" + exception.getMessage());
			return null;
		} catch (Exception exception) {
			System.out.println("ERROR:\n" + exception.getMessage());
			return null;
		}
	}
}
