import java.io.Serializable;

/**
 * Abstract class Appliance is the superclass for all appliances in the company.
 * Contains the shared methods and attributes.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public abstract class Appliance implements Serializable, Comparable<Appliance> {
	private static final long serialVersionUID = 1L;
	protected final String brand;
	protected final String model;
	protected final double price;

	/**
	 * Constructor for Appliance.
	 * 
	 * @param brand
	 * @param model
	 * @param price
	 */
	public Appliance(String brand, String model, double price) {
		this.brand = brand;
		this.model = model;
		this.price = price;
	}

	/**
	 * getBrand(): String
	 * 
	 * @return the brand name of appliance
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * getPrice(): double
	 * 
	 * @return the price of the appliance
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * getModel(): String
	 * 
	 * @return the model name of the appliance
	 */
	public String getModel() {
		return model;
	}

	/**
	 * hashCode(): int
	 * 
	 * Overrides hash code method to provide a hash algorithm reflecting equality.
	 * 
	 * @return the hash code of this appliance
	 */
	@Override
	public int hashCode() {
		return brand.hashCode() + model.hashCode() + 53;
	}

	/**
	 * equals(object: Object): boolean
	 * 
	 * defines equality for Appliance objects
	 * 
	 * @param object - the object we are testing for equality
	 * @return true if the two objects are equal as defined by this method's
	 *         implementation; false otherwise
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null || getClass() != object.getClass()) {
			return false;
		} else {
			Appliance other = (Appliance) object;
			return this.compareTo(other) == 0;
		}
	}

	/**
	 * toString(): String
	 * 
	 * @return a description of the appliance
	 */
	@Override
	public String toString() {
		return "Brand: " + getBrand() + "\tModel: " + getModel() + "\tPrice: " + getPrice();
	}

	/**
	 * info(): String
	 * 
	 * A method to just return the brand and model of the appliance.
	 * 
	 * @return brand and model in String format
	 */
	public String info() {
		return "Brand: " + getBrand() + "\tModel: " + getModel();
	}

	/**
	 * compareTo(other: Appliance): int
	 * 
	 * This method compares two appliances and returns an integer value that
	 * provides a way of ordering the appliances in a collection. They are compared
	 * by their String values of brand and model.
	 * 
	 * @param other - a reference to the other Appliance object we are comparing
	 * @return an integer value representing the difference
	 */
	@Override
	public int compareTo(Appliance other) {
		if (!brand.equals(other.getBrand())) {
			return brand.compareTo(other.getBrand());
		} else {
			return model.compareTo(other.getModel());
		}
	}

	/**
	 * match(brand: String, model: String): boolean
	 * 
	 * A method to test for equality given just the brand and model. This method was
	 * created to avoid creating new objects to test for equality.
	 * 
	 * @param brand
	 * @param model
	 * @return true if equal
	 */
	public boolean match(String brand, String model) {
		return (this.brand.compareTo(brand) == 0 && this.model.compareTo(model) == 0);
	}

	/**
	 * accept(visitor: ApplianceVisitor): String
	 * 
	 * A method to accept a visitor to this appliance.
	 * 
	 * @param visitor - an object of a class that implements ApplianceVisitor
	 * @return a String object created by this visitor
	 */
	public String accept(ApplianceVisitor visitor) {
		return visitor.visit(this);
	}
}
