import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * ApplianceCompany is a singleton class and is serializable. Maintains 3
 * collections of customers, washers, and backorders. Fields
 * totalInPurchaseSales and totalInRepairPlanSales keeps track of the amount
 * billed for purchases and repair plans respectively. All public methods, save
 * the instance() method, relate to a specific use case including loading a file
 * from disk.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class ApplianceCompany implements Serializable {
	private static final long serialVersionUID = 1L;
	private static ApplianceCompany company;
	private Customers customers;
	private Inventory inventory;
	private BackOrders backOrders;
	private double totalInPurchaseSales;
	private double totalInRepairPlanSales;
	public static final int DISHWASHER = 1;
	public static final int DRYER = 2;
	public static final int FURNACE = 3;
	public static final int KITCHEN_RANGE = 4;
	public static final int REFRIGERATOR = 5;
	public static final int WASHER = 6;

	/**
	 * Private constructor for ApplianceCompany singleton. Only called from within
	 * the instance() method.
	 */
	private ApplianceCompany() {
		customers = new Customers();
		inventory = new Inventory();
		backOrders = new BackOrders();
	}

	/**
	 * instance(): ApplianceCompany
	 * 
	 * @return - the only ApplianceCompany instance() allowed to be created or
	 *         loaded from a file by this class.
	 */
	public static ApplianceCompany instance() {
		if (company == null) {
			company = new ApplianceCompany();
		}
		return company;
	}

	/**
	 * addCustomer(name: String, phoneNumber: String): boolean
	 * 
	 * Attempts to add a customer to our customer collection.
	 * 
	 * @param name        - the name of customer
	 * @param phoneNumber - the phone number of customer
	 * @return true if customer was able to be created, false otherwise (id already
	 *         exists)
	 */
	public boolean addCustomer(String name, String phoneNumber) {
		return customers.addCustomer(new Customer(name, phoneNumber));
	}

	/**
	 * addModel(brand: String, model: String, price: double, monthlyPayment: double,
	 * capacity: double, btu: double): boolean
	 * 
	 * Attempts to add an appliance to the company.
	 * 
	 * @param brand          - the brand name of appliance
	 * @param model          - the model name of appliance
	 * @param price          - the price of appliance
	 * @param monthlyPayment - the monthly payment for repair plan of appliance
	 * @param capacity       - the capacity of appliance (refrigerator)
	 * @param btu            - the btu of appliance (furnace)
	 * @return true if an appliance was added successfully, false if price provided
	 *         was negative
	 */
	public boolean addModel(int type, String brand, String model, double price, double monthlyPayment, double capacity,
			double btu) {
		return inventory.addModel(
				ApplianceFactory.instance().createAppliance(type, brand, model, price, monthlyPayment, capacity, btu));
	}

	/**
	 * addToInventory(brand: String, model: String, quantity: int): int
	 * 
	 * Attempts to add the quantity of a specific appliance in our inventory.
	 * Updates totalSales if their were back-orders and then will update inventory.
	 * 
	 * @param brand    - the brand name of appliance
	 * @param model    - the model name of appliance
	 * @param quantity - the amount added to stock
	 * @return the number of backorders completed
	 * 
	 */
	public int addToInventory(String brand, String model, int quantity) {
		Appliance appliance = inventory.search(brand, model);
		if (appliance != null && quantity > 0) {
			int quantityUsed = backOrders.updateBackOrders(appliance, quantity);
			totalInPurchaseSales += quantityUsed * appliance.getPrice();
			inventory.updateQuantity(appliance, quantity - quantityUsed);
			return quantityUsed;
		} else {
			return UserInterface.INVALID_INPUT;
		}
	}

	/**
	 * purchase(brand: String, model: String, customerId: int, quantity: int): int
	 * 
	 * Attempts to purchase appliances for specific customers. Variety of possible
	 * outcomes of this method. There is enough in stock to satisfy a complete
	 * order. The order can only be completed partially due to not having enough in
	 * stock, and the remaining are placed on backorder. We are out of stock and the
	 * entire order is placed on backorder, or the appliance or customer id provided
	 * does not match information in our system. Or we are out of stock and the
	 * appliance provided cannot be placed on backorder.
	 * 
	 * @param brand      - the brand name of the appliance
	 * @param model      - the model name of the appliance
	 * @param customerId - the customer id
	 * @param quantity   - the amount wanted
	 * @return the quantity sold, returns -1 if appliance or customer was not found
	 */
	public int purchase(String brand, String model, int customerId, int quantity) {
		if (quantity <= 0) {
			return UserInterface.INVALID_INPUT;
		}
		Appliance appliance = inventory.search(brand, model);
		Customer customer = customers.searchById(customerId);
		if (appliance != null && customer != null) {
			int quantityInStock = inventory.getQuantity(appliance);
			if (quantityInStock == 0 && appliance instanceof Furnace) {
				return -2;
			}
			int quantitySold;
			// if quantity desired is less than or equal to stock on hand
			if (quantity <= quantityInStock) {
				inventory.updateQuantity(appliance, -quantity);
				quantitySold = quantity;
			} else {
				if (appliance instanceof Furnace) {
					return -2;
				}
				// not enough in stock, we will sell what is left and place the rest on
				// back-order
				int quantityBackOrdered = quantity - quantityInStock;
				quantitySold = quantityInStock;
				inventory.updateQuantity(appliance, -quantityInStock);
				backOrders.addBackOrder(new BackOrder(customer, appliance, quantityBackOrdered));
			}
			double charge = quantitySold * appliance.getPrice();
			totalInPurchaseSales += charge;
			customer.increasePurchases(charge);
			return quantitySold;
		} else {
			return UserInterface.INVALID_INPUT;
		}
	}

	/**
	 * listCustomers(): String
	 * 
	 * @return customer list in String format
	 */
	public String listCustomers() {
		return customers.toString();
	}

	/**
	 * listOf(type: int): String
	 * 
	 * Returns the list of appliances of the type provided in the parameter.
	 * 
	 * @param type    - an integer indicating the appliance type
	 * @param visitor - an object that implements the ApplianceVisitor interface
	 *                that defines the format
	 * @return A list in String format of the appliances in inventory
	 */
	public String listOf(int type, ApplianceVisitor visitor) {
		if (type == 0) {
			return inventory.toString();
		}
		return inventory.listOf(type, visitor);
	}

	/**
	 * getTotalSales(): String
	 * 
	 * @return the total sales in purchases and repair plans in String format
	 */
	public String getTotalSales() {
		return String.format("Total Sales: $%.2f%nTotal in Service Charges: $%.2f", totalInPurchaseSales,
				totalInRepairPlanSales);
	}

	/**
	 * enrollInRepairPlan(brand: String, model: String, customerId: int): boolean
	 * 
	 * Searches the inventory and customer list for the appliance and customer and
	 * attempts to enroll the customer in a repair plan by calling the appropriate
	 * method in Customers.
	 * 
	 * @param brand      - the brand of the appliance
	 * @param model      - the model of the appliance
	 * @param customerId - the id of the customer
	 * @return true if successful enrollment
	 */
	public boolean enrollInRepairPlan(String brand, String model, int customerId) {
		Customer customer = customers.searchById(customerId);
		Appliance appliance = inventory.search(brand, model);
		if (customer != null && appliance != null) {
			if (appliance instanceof ServicePlanProvider) {
				return customers.enrollInRepairPlan(customer, (ServicePlanProvider) appliance);
			}
		}
		return false;
	}

	/**
	 * withdrawFromRepairPlan(brand: String, model: String, customerId: int):
	 * boolean
	 * 
	 * Searches the inventory and customer list for the appliance and customer and
	 * attempts to withdraw the customer's enrollment by calling the appropriate
	 * method in Customers.
	 * 
	 * @param brand      - the brand of the appliance
	 * @param model      - the model of the appliance
	 * @param customerId - the id of the customer
	 * @return true if successful withdrawal
	 * @return
	 */
	public boolean withdrawFromRepairPlan(String brand, String model, int customerId) {
		Customer customer = customers.searchById(customerId);
		Appliance appliance = inventory.search(brand, model);
		if (customer != null && appliance != null) {
			if (appliance instanceof ServicePlanProvider) {
				return customers.withdrawFromRepairPlan(customer, (ServicePlanProvider) appliance);
			}
		}
		return false;

	}

	/**
	 * billRepairPlans(): double
	 * 
	 * Bills all customers enrolled in a repair plan and returns the amount billed.
	 * 
	 * @return the total amount billed to all customers
	 */
	public double billRepairPlans() {
		Customer customer;
		double amountCollected = 0.0;
		for (Iterator<Customer> iterator = customers.getItems(); iterator.hasNext();) {
			customer = iterator.next();
			TreeSet<ServicePlanProvider> currentlyEnrolledPlans = customers.get(customer);
			for (ServicePlanProvider appliance : currentlyEnrolledPlans) {
				customer.increaseRepairPlanAmount(appliance.getMonthlyPaymentAmount());
				amountCollected += appliance.getMonthlyPaymentAmount();
			}
		}
		totalInRepairPlanSales += amountCollected;
		return amountCollected;
	}

	/**
	 * listUsersInRepairPlans(): String
	 * 
	 * Returns a list of customers that are enrolled in repair plans according to
	 * the fixed String format.
	 * 
	 * @return the list as a String
	 */
	public String listUsersInRepairPlans() {
		return customers.listUsersInRepairPlans();
	}

	/**
	 * listAllBackOrders(): String
	 * 
	 * Returns a list of all backorders as a String formatted ordered by appliance
	 * with the appliance displayed as defined in ApplianceDisplayBackOrderFormat.
	 * 
	 * @return the list of backorders as a String
	 */
	public String listAllBackOrders() {
		return backOrders.listOfBackOrders();
	}

	/**
	 * save(filename: String): boolean
	 * 
	 * Will save the company object to the file with the name specified in its
	 * parameter.
	 * 
	 * @param filename - the name of the file
	 * @return true if data was successfully saved, false if not
	 */
	public boolean save(String filename) {
		try (ObjectOutputStream data = new ObjectOutputStream(new FileOutputStream(filename))) {
			data.writeObject(company);
			return true;
		} catch (FileNotFoundException ex) {
			System.out.println("The file could not be created.\n" + ex.getMessage());
			return false;
		} catch (InvalidClassException ex) {
			System.out.println("Something is wrong with a class used by serialization.\n" + ex.getMessage());
			return false;
		} catch (IOException ex) {
			System.out.println("An I/O error has occurred.\n" + ex.getMessage());
			return false;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return false;
		}
	}

	/**
	 * load(fileName: String): Object
	 * 
	 * Given a filename this method will open the file and retrieve the object
	 * stored on it. If a ApplianceCompany object already exists in memory then it
	 * will ignore the file and inform the user. Any errors/exceptions are also
	 * printed.
	 * 
	 * @param filename - the name of the file that contains the object
	 * @return returns the ApplianceCompany singleton whether obtained from disk or
	 *         the one already in use.
	 */
	public static Object load(String filename) {
		try (ObjectInputStream data = new ObjectInputStream(new FileInputStream(filename))) {
			if (company == null) {
				company = (ApplianceCompany) data.readObject();
				if (company.customers.retrieveLastId() > 0) {
					Customer.setCount(company.customers.retrieveLastId());
				}
			} else {
				System.out.println("You cannot load from file.  Company already exists.");
			}
			return company;
		} catch (FileNotFoundException exception) {
			System.out.println("The file could not be created.\n" + exception.getMessage());
			return null;
		} catch (ClassNotFoundException exception) {
			System.out.println("Something is wrong with a class used by serialization.\n" + exception.getMessage());
			return null;
		} catch (IOException exception) {
			System.out.println("An I/O error has occurred.\n" + exception.getMessage());
			return null;
		} catch (Exception exception) {
			System.out.println("ERROR:\n" + exception.getMessage());
			return null;
		}
	}
}
