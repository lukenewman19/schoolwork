import java.io.Serializable;

/**
 * ApplianceDisplayBackOrderFormat class implements ApplianceVisitor to
 * construct its own String displays for each type of appliance. It is used in
 * displaying backorders.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class ApplianceDisplayBackOrderFormat implements ApplianceVisitor, Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * A catch all method for unknown appliance types.
	 */
	@Override
	public String visit(Appliance appliance) {
		return "";

	}

	/**
	 * A method to deal with Dishwasher objects.
	 */
	@Override
	public String visit(Dishwasher dishwasher) {
		return String.format("Dishwasher\tBrand: %-20sModel: %-20sPrice: %.2f", dishwasher.getBrand(),
				dishwasher.getModel(), dishwasher.getPrice());
	}

	/**
	 * A method to deal with Dryer objects.
	 */
	@Override
	public String visit(Dryer dryer) {
		return String.format("Dryer\tBrand: %-20sModel: %-20sPrice: %-10.2f\tMonthly Payment: %.2f", dryer.getBrand(),
				dryer.getModel(), dryer.getPrice(), dryer.getMonthlyPaymentAmount());
	}

	/**
	 * A method to deal with Furnace objects.
	 */
	@Override
	public String visit(Furnace furnace) {
		return String.format("Furnace\t Brand: %-20sModel: %-20sPrice: %-10.2f\tMax Heat Output (BTU): %.2f",
				furnace.getBrand(), furnace.getModel(), furnace.getPrice(), furnace.getMaximumHeatingOutput());
	}

	/**
	 * A method to deal with KitchenRange objects.
	 */
	@Override
	public String visit(KitchenRange kitchenRange) {
		return String.format("KitchenRange\tBrand: %-20sModel: %-20sPrice: %.2f", kitchenRange.getBrand(),
				kitchenRange.getModel(), kitchenRange.getPrice());
	}

	/**
	 * A method to deal with Refrigerator objects.
	 */
	@Override
	public String visit(Refrigerator refrigerator) {
		return String.format("Refrigerator\tBrand: %-20sModel: %-20sPrice: %-10.2f\tCapacity (Liters): %.2f",
				refrigerator.getBrand(), refrigerator.getModel(), refrigerator.getPrice(), refrigerator.getCapacity());

	}

	/**
	 * A method to deal with Washer objects.
	 */
	@Override
	public String visit(Washer washer) {
		return String.format("Washer\tBrand: %-20sModel: %-20sPrice: %-10.2f\tMonthly Payment: %.2f", washer.getBrand(),
				washer.getModel(), washer.getPrice(), washer.getMonthlyPaymentAmount());

	}

	/**
	 * This format does not need to inform the client of the type.
	 */
	public String getType(int type) {
		return "";
	}

	/**
	 * This format does not use a header. It is not presented in column format.
	 */
	public String getHeader(int type) {
		return "";
	}

}
