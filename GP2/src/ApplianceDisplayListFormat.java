import java.io.Serializable;

/**
 * ApplianceDisplayBackOrderFormat class implements ApplianceVisitor to
 * construct its own String displays for each type of appliance. It is used in
 * displaying the appliances when listing only a specific type.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class ApplianceDisplayListFormat implements ApplianceVisitor, Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * A catch all method for unknown appliance types.
	 */
	@Override
	public String visit(Appliance appliance) {
		return "";

	}

	/**
	 * A method to deal with Dishwasher objects.
	 */
	@Override
	public String visit(Dishwasher dishwasher) {
		return String.format("%-20s%-20s%.2f", dishwasher.getBrand(), dishwasher.getModel(), dishwasher.getPrice());
	}

	/**
	 * A method to deal with Dryer objects.
	 */
	@Override
	public String visit(Dryer dryer) {
		return String.format("%-20s%-20s%-10.2f\t%.2f", dryer.getBrand(), dryer.getModel(), dryer.getPrice(),
				dryer.getMonthlyPaymentAmount());
	}

	/**
	 * A method to deal with Furnace objects.
	 */
	@Override
	public String visit(Furnace furnace) {
		return String.format("%-20s%-20s%-10.2f\t%.2f", furnace.getBrand(), furnace.getModel(), furnace.getPrice(),
				furnace.getMaximumHeatingOutput());
	}

	/**
	 * A method to deal with KitchenRange objects.
	 */
	@Override
	public String visit(KitchenRange kitchenRange) {
		return String.format("%-20s%-20s%.2f", kitchenRange.getBrand(), kitchenRange.getModel(),
				kitchenRange.getPrice());
	}

	/**
	 * A method to deal with Refrigerator objects.
	 */
	@Override
	public String visit(Refrigerator refrigerator) {
		return String.format("%-20s%-20s%-10.2f\t%.2f", refrigerator.getBrand(), refrigerator.getModel(),
				refrigerator.getPrice(), refrigerator.getCapacity());

	}

	/**
	 * A method to deal with washer objects.
	 */
	@Override
	public String visit(Washer washer) {
		return String.format("%-20s%-20s%-10.2f\t%.2f", washer.getBrand(), washer.getModel(), washer.getPrice(),
				washer.getMonthlyPaymentAmount());

	}

	/**
	 * A method to return the corresponding header in String format. Appliance types
	 * vary in their columns.
	 */
	public String getHeader(int type) {
		String header = "";
		switch (type) {
		case ApplianceCompany.DISHWASHER:
			header = "Dishwashers in inventory\nBrand\t\t\tModel\t\tPrice $\t\tQuantity\t\t\n"
					+ "--------------------------------------------------------------------\n";
			break;
		case ApplianceCompany.KITCHEN_RANGE:
			header = "KitchenRanges in inventory\n Brand\t\t\tModel\t\tPrice $\t\tQuantity\t\t\n"
					+ "------------------------------------------------------------------\n";
			break;
		case ApplianceCompany.WASHER:
			header = "Washers in inventory\n Brand\t\t\tModel\t\tPrice $\t\tMonthlyPayment $\tQuantity\n"
					+ "-----------------------------------------------------------------------------------\n";
			break;
		case ApplianceCompany.DRYER:
			header = "Dryers in inventory\n Brand\t\t\tModel\t\tPrice $\t\tMonthlyPayment $\tQuantity\n"
					+ "-----------------------------------------------------------------------------------\n";
			break;
		case ApplianceCompany.FURNACE:
			header = "Furnaces in inventory\n Brand\t\t\tModel\t\tPrice $\t\tMaximumHeatingOutput (BTU)\tQuantity\n"
					+ "-----------------------------------------------------------------------------------------\n";
			break;
		case ApplianceCompany.REFRIGERATOR:
			header = "Refrigerators in inventory\n Brand\t\t\tModel\t\tPrice $\t\tCapacity (Liters)\tQuantity\n"
					+ "-------------------------------------------------------------------------------------\n";
			break;
		}
		return header;
	}

	/**
	 * A method to return the corresponding type in String format. Used to get the
	 * String format of the user request.
	 */
	public String getType(int type) {
		String stringType = "";
		switch (type) {
		case ApplianceCompany.DISHWASHER:
			stringType = "Dishwasher";
			break;
		case ApplianceCompany.KITCHEN_RANGE:
			stringType = "KitchenRange";
			break;
		case ApplianceCompany.WASHER:
			stringType = "Washer";
			break;
		case ApplianceCompany.DRYER:
			stringType = "Dryer";
			break;
		case ApplianceCompany.FURNACE:
			stringType = "Furnace";
			break;
		case ApplianceCompany.REFRIGERATOR:
			stringType = "Refrigerator";
			break;
		}
		return stringType;
	}

}
