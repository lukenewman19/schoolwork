import java.io.Serializable;

/**
 * Creates different types of Appliance objects. When a new Appliance is
 * introduced, the constructor for that class must be invoked from here. This is
 * a singleton.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class ApplianceFactory implements Serializable {
	private static final long serialVersionUID = 1L;
	private static ApplianceFactory factory;

	/**
	 * Private for singleton
	 */
	private ApplianceFactory() {
	}

	/**
	 * Returns the only instance of the class.
	 * 
	 * @return the instance
	 */
	public static ApplianceFactory instance() {
		if (factory == null) {
			factory = new ApplianceFactory();
		}
		return factory;
	}

	/**
	 * Creates a Appliance object and returns it.
	 * 
	 * @param type           the type of the appliance
	 * @param brand          the brand of the appliance
	 * @param model          the model of the appliance
	 * @param price          the price of the appliance
	 * @param monthlyPayment the monthly payment of the appliance (if it is a washer
	 *                       or drier)
	 * @param capacity       the capacity of the appliance (if it is a refrigerator)
	 * @param btu            the btu of the appliance (if it is a furnace)
	 * @return the item that was created
	 */
	public Appliance createAppliance(int type, String brand, String model, double price, double monthlyPayment,
			double capacity, double btu) {
		switch (type) {
		case ApplianceCompany.DISHWASHER:
			return new Dishwasher(brand, model, price);
		case ApplianceCompany.DRYER:
			return new Dryer(brand, model, price, monthlyPayment);
		case ApplianceCompany.FURNACE:
			return new Furnace(brand, model, price, btu);
		case ApplianceCompany.KITCHEN_RANGE:
			return new KitchenRange(brand, model, price);
		case ApplianceCompany.REFRIGERATOR:
			return new Refrigerator(brand, model, price, capacity);
		case ApplianceCompany.WASHER:
			return new Washer(brand, model, price, monthlyPayment);
		default:
			return null;
		}
	}
}
