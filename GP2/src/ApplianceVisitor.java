import java.io.Serializable;

/**
 * ApplianceVisitor is an interface that provides the methods necessary to
 * differentiate what to do based on what type of Appliance is passed into its
 * visit() method.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public interface ApplianceVisitor extends Serializable {
	public String visit(Appliance appliance);

	public String visit(Dishwasher dishwasher);

	public String visit(Dryer dryer);

	public String visit(Furnace furnace);

	public String visit(KitchenRange kitchenRange);

	public String visit(Refrigerator refrigerator);

	public String visit(Washer washer);

	public String getHeader(int type);

	public String getType(int type);
}
