import java.io.Serializable;

/**
 * BackOrder represents an order waiting to be fulfilled as soon as the desired
 * Washer is in stock. Each BackOrder contains a Customer and Washer object as
 * well as a quantity.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class BackOrder implements Serializable {
	private static final long serialVersionUID = 1L;
	Customer customer;
	Appliance appliance;
	int quantity;

	/**
	 * Constructor for BackOrder.
	 * 
	 * Instantiates all three instance fields.
	 * 
	 * @param customer - the customer that placed the order
	 * @param washer   - the washer that the customer is requesting
	 * @param quantity - the number of washers ordered
	 */
	public BackOrder(Customer customer, Appliance appliance, int quantity) {
		this.customer = customer;
		this.appliance = appliance;
		this.quantity = quantity;
	}

	/**
	 * getCustomer(): Customer
	 * 
	 * @return a reference to the customer object who placed the order
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * getWasher(): Washer
	 * 
	 * @return a reference to the washer object in this order
	 */
	public Appliance getAppliance() {
		return appliance;
	}

	/**
	 * getQuantity(): int
	 * 
	 * @return the quantity of washers in this order
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * setQuantity(quantity: int): void
	 * 
	 * Sets the quantity of washers in this order. This method is used for partial
	 * backorders where it is possible that the quantity must be adjusted after a
	 * partial amount of the order is processed.
	 * 
	 * @param quantity - the new quantity of washers in this order
	 */
	public void setQuantity(int quantity) {
		if (quantity > 0) {
			this.quantity = quantity;
		}
	}

	/**
	 * toString(): String
	 * 
	 * @return a description of the backorder
	 */
	@Override
	public String toString() {
		return getCustomer() + ", " + appliance + ", " + "Quantity: " + quantity;
	}
}
