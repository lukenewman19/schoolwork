import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * BackOrders extends generic collection class ItemList<T, K> with Appliances as
 * keys (T) and a queue of BackOrders as their respective values.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class BackOrders extends ItemList<Appliance, Queue<BackOrder>> implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * addBackOrder(order: BackOrder): void
	 * 
	 * Adds a BackOrder object to the Queue value associated with the key. The key
	 * is the appliance object which is kept by the back-order. The Queue's offer
	 * method is used to add a back-order.
	 * 
	 * @param order - the backorder to be offered into the queue
	 */
	public void addBackOrder(BackOrder order) {
		Appliance appliance = order.getAppliance();
		Queue<BackOrder> queue = get(appliance);
		if (queue != null) {
			queue.offer(order);
		} else {
			// initially this collection is empty
			// keys and values are created one at a time as each washer is encountered.
			Queue<BackOrder> value = new LinkedList<>();
			value.offer(order);
			add(appliance);
			updateValue(appliance, value);
		}
	}

	/**
	 * updateBackOrders(appliance: Appliance, quantity: int): int
	 * 
	 * Checks the queue of backorders for the appliance object passed in and
	 * attempts to complete and remove the backorders from the queue. Backorders are
	 * only removed from queue if there is enough passed in as quantity to satisfy
	 * the order. Backorders can be partially processed and kept in the front of the
	 * queue.
	 * 
	 * @param appliance - the appliance whose backorders we are to check for
	 * @param quantity  - the amount updated in inventory
	 * @return the quantity used up from backorders
	 */
	public int updateBackOrders(Appliance appliance, int quantity) {
		int quantityUsed = 0;
		int quantityRemained = quantity;
		Queue<BackOrder> orders = get(appliance);
		// return 0 if the washer contains no orders, the value of orders would be null
		if (orders == null) {
			return 0;
		}
		while (quantityRemained > 0 && !orders.isEmpty()) {
			BackOrder orderToUpdate = orders.peek();
			Customer customer = orderToUpdate.getCustomer();
			if (quantityRemained >= orderToUpdate.getQuantity()) {
				double customerCharge = orderToUpdate.getQuantity() * appliance.getPrice();
				quantityUsed += orders.remove().getQuantity();
				quantityRemained = quantity - quantityUsed;
				customer.increasePurchases(customerCharge);
			} else {
				orderToUpdate.setQuantity(orderToUpdate.getQuantity() - quantityRemained);
				quantityUsed += quantityRemained;
				quantityRemained = 0;
				customer.increasePurchases(quantityRemained * appliance.getPrice());
			}
		}
		return quantityUsed;
	}

	/**
	 * listOfBackOrders(): String
	 * 
	 * Returns the list of BackOrders ordered by appliance with the appliance
	 * displayed as defined in ApplianceDisplayBackOrderFormat.
	 * 
	 * @return the list of backorders in String format
	 */
	public String listOfBackOrders() {
		Appliance appliance;
		Queue<BackOrder> orders;
		BackOrder order;
		String list = "BackOrders \n";
		ApplianceVisitor visitor = new ApplianceDisplayBackOrderFormat();
		for (Iterator<Appliance> iterator = getItems(); iterator.hasNext();) {
			appliance = iterator.next();
			orders = get(appliance);
			if (!orders.isEmpty()) {
				list += "Ordered for appliance:\n" + appliance.accept(visitor)
						+ "\n\tCustomer Name\tCustomer Id\t Quantity on BackOrder\n";
				Iterator<BackOrder> it = orders.iterator();
				while (it.hasNext()) {
					order = it.next();
					list += "\t" + String.format("%10s%12s\t\t%12s%n", order.getCustomer().getName(),
							order.getCustomer().getId(), order.getQuantity());
				}
			}
		}
		return list;
	}
}
