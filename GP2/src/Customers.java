import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Customers class extends generic collection class ItemList<T, K>. Customers
 * serve as keys (T in this case) and a TreeSet of Appliances that implement the
 * ServicePlanProvider interface (K) represent the values of each key.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Customers extends ItemList<Customer, TreeSet<ServicePlanProvider>> implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * addCustomer(customer: Customer): boolean
	 * 
	 * Adds a customer to the customer collection. Customers with the same id as an
	 * existing customer cannot be added.
	 * 
	 * @param customer - the customer to be added
	 * @return true if addition was successful, false otherwise (duplicate id)
	 */
	public boolean addCustomer(Customer customer) {
		if (add(customer)) {
			return updateValue(customer, new TreeSet<ServicePlanProvider>());
		} else {
			return false;
		}

	}

	/**
	 * searchById(customerId: int): Customer
	 * 
	 * Searches the collection for a customer with the customer id provided by the
	 * method's argument
	 * 
	 * @param customerId - the integer id of the customer
	 * @return a reference to the customer object
	 */
	public Customer searchById(int customerId) {
		Customer customer = null;
		for (Iterator<Customer> iterator = getItems(); iterator.hasNext();) {
			customer = iterator.next();
			if (customer.getId() == customerId) {
				return customer;
			}
		}
		return null;
	}

	/**
	 * enrollInRepairPlan(customer: Customer, appliance: ServicePlanProvider):
	 * boolean
	 * 
	 * Attempts to add an appliance to the set of appliances that the user is
	 * enrolled with. The Customer argument provided must be a valid customer or
	 * this method will throw a null pointer exception.
	 * 
	 * @param customer  - the customer to be enrolled
	 * @param appliance - the appliance associated with the repair plan
	 * @return true is successful enrollment
	 */
	public boolean enrollInRepairPlan(Customer customer, ServicePlanProvider appliance) {
		TreeSet<ServicePlanProvider> currentlyEnrolledPlans = get(customer);
		return currentlyEnrolledPlans.add(appliance);
	}

	/**
	 * withdrawFromRepairPlan(customer: Customer, appliance: ServicePlanProvider):
	 * boolean
	 * 
	 * Attempts to remove the appliance from the set of appliances that the user is
	 * enrolled with. The Customer argument provided must be a valid customer in the
	 * collection or this method will throw a null pointer exception.
	 * 
	 * @param customer  - the customer withdrawing
	 * @param appliance - the appliance associated with the repair plan
	 * @return true is successful withdrawal
	 */
	public boolean withdrawFromRepairPlan(Customer customer, ServicePlanProvider appliance) {
		TreeSet<ServicePlanProvider> currentlyEnrolledPlans = get(customer);
		return currentlyEnrolledPlans.remove(appliance);
	}

	/**
	 * retrieveLastId(): int
	 * 
	 * This method obtains the last customer in the set and returns its id. When
	 * loading from file this method is called to set the static field count in
	 * Customer for id generation. Since we increment id numbers by one for each new
	 * customer, this is the only information we need to add new customers to a
	 * ApplianceCompany that has been loaded from a file.
	 * 
	 * @return the last id number in the customer set
	 */
	public int retrieveLastId() {
		Customer lastCustomer = getLast();
		if (lastCustomer == null) {
			return 0;
		}
		return lastCustomer.getId();
	}

	/**
	 * listUsersInRepairPlans(): String
	 * 
	 * Returns a list of all customers enrolled in repair plans including the
	 * appliances associated with the plan. We have set a limit of one repair plan
	 * per appliance type for now.
	 * 
	 * @return the list as a String
	 */
	public String listUsersInRepairPlans() {
		String customerList = "Name\t\tPhone Number\tId\tTotal Purchases $ \tTotal Service Charges $\n"
				+ "----------------------------------------------------------------------------\n";
		Customer customer;
		for (Iterator<Customer> iterator = getItems(); iterator.hasNext();) {
			customer = iterator.next();
			TreeSet<ServicePlanProvider> appliances = get(customer);
			if (appliances != null && appliances.size() > 0) {
				customerList += String.format("%n%-12s\t%-10s\t%d\t%-12.2f\t\t%-10.2f%n", customer.getName(),
						customer.getPhoneNumber(), customer.getId(), customer.getPurchasesAmount(),
						customer.getRepairPlanAmount());
				customerList += "\tAppliances Enrolled in:\n";
				for (ServicePlanProvider appliance : appliances) {
					customerList += "\t" + appliance.info() + "\n";
				}
			}
		}
		return customerList;
	}

	/**
	 * toString(): String
	 * 
	 * Returns the list of customers, one customer per line
	 */
	public String toString() {
		String customersList = "Name\t\tPhone Number\tId\tEnrolled\n" + "---------------------------------------\n";
		Customer customer;
		for (Iterator<Customer> iterator = getItems(); iterator.hasNext();) {
			customer = iterator.next();
			customersList += String.format("%-12s\t%-10s\t%d\t%-10s%n", customer.getName(), customer.getPhoneNumber(),
					customer.getId(), !get(customer).isEmpty());
		}
		return customersList;
	}
}
