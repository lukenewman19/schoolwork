import java.io.Serializable;

/**
 * Dishwasher class extends Appliance with no additional attributes and an
 * accept method to accept a visitor.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Dishwasher extends Appliance implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for Dishwasher objects. Invokes super constructor.
	 * 
	 * @param brand
	 * @param model
	 * @param price
	 */
	public Dishwasher(String brand, String model, double price) {
		super(brand, model, price);
	}

	/**
	 * accept(visitor: ApplianceVisitor): String
	 * 
	 * Dishwasher accepts an object that implements the ApplianceVisitor interface.
	 * The visitor visits this object by passing it into its visit() method.
	 * 
	 * @return the String created by the visitor object
	 */
	public String accept(ApplianceVisitor visitor) {
		return visitor.visit(this);
	}

}
