import java.io.Serializable;

/**
 * Dryer class extends Appliance with the additional attribute
 * monthlyPaymentAmount. It implements ServicePlanProvider which means that it
 * provides the methods that are needed for that type of appliance.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Dryer extends Appliance implements Serializable, ServicePlanProvider {
	private static final long serialVersionUID = 1L;
	private double monthlyPaymentAmount;

	/**
	 * Constructor for Dryer.
	 * 
	 * @param brand   - the brand name of washer
	 * @param model   - the model name of washer
	 * @param price   - the price of washer
	 * @param monthly - monthly payment amount
	 */
	public Dryer(String brand, String model, double price, double monthly) {
		super(brand, model, price);
		monthlyPaymentAmount = monthly;
	}

	/**
	 * Getter method for monthly payment amount.
	 * 
	 * @return the monthly payment amount
	 */
	public double getMonthlyPaymentAmount() {
		return monthlyPaymentAmount;
	}

	/**
	 * accept(visitor: ApplianceVisitor): String
	 * 
	 * Dryer accepts an object that implements the ApplianceVisitor interface. The
	 * visitor visits this object by passing it into its visit() method.
	 * 
	 * @return the String created by the visitor object
	 */
	public String accept(ApplianceVisitor visitor) {
		return visitor.visit(this);
	}
}
