import java.io.Serializable;

/**
 * Furnace class extends Appliance with the additional attribute
 * maximumHeatingOutput.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Furnace extends Appliance implements Serializable {
	private static final long serialVersionUID = 1L;
	// BTU
	private double maximumHeatingOutput;

	/**
	 * Constructor for Furnace.
	 * 
	 * @param brand
	 * @param model
	 * @param price
	 * @param maximumHeatingOutput
	 */
	public Furnace(String brand, String model, double price, double maximumHeatingOutput) {
		super(brand, model, price);
		this.maximumHeatingOutput = maximumHeatingOutput;
	}

	/**
	 * Getter method for maximum heating output.
	 * 
	 * @return the maximum heating output of this furnace
	 */
	public double getMaximumHeatingOutput() {
		return maximumHeatingOutput;
	}

	/**
	 * accept(visitor: ApplianceVisitor): String
	 * 
	 * Furnace accepts an object that implements the ApplianceVisitor interface. The
	 * visitor visits this object by passing it into its visit() method.
	 * 
	 * @return the String created by the visitor object
	 */
	public String accept(ApplianceVisitor visitor) {
		return visitor.visit(this);
	}
}
