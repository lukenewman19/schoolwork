import java.io.Serializable;
import java.util.Iterator;

/**
 * Inventory class extends generic collection class ItemList<T, K> with
 * Appliances as the key (T) and an integer object for the value (K)
 * representing the quantity in stock.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Inventory extends ItemList<Appliance, Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * addModel(appliance: Appliance): boolean
	 * 
	 * Adds an appliance model to the inventory.
	 * 
	 * @param appliance - appliance to be added
	 * @return true if addition was successful, false if appliance type is already
	 *         in the inventory
	 */
	public boolean addModel(Appliance appliance) {
		if (get(appliance) != null) {
			return false;
		}
		add(appliance);
		updateValue(appliance, 0);
		return true;
	}

	/**
	 * updateQuantity(appliance: Appliance, quantity: int): boolean
	 * 
	 * Updates the quantity in inventory of the appliance type passed in. Negative
	 * integers may be used to decrease the quantity such as after a sale. If the
	 * appliance is not found in the collection then the method returns false.
	 * 
	 * @param appliance - appliance whose quantity is to be updated
	 * @param quantity  - the amount by which to increase or decrease the inventory
	 * @return true if update succeeded, false if appliance not found
	 */
	public boolean updateQuantity(Appliance appliance, int quantity) {
		if (get(appliance) == null) {
			return false;
		}
		quantity += get(appliance);
		return updateValue(appliance, quantity);
	}

	/**
	 * getQuantity(appliance: Appliance): int
	 * 
	 * Returns the quantity in inventory of the appliance.
	 * 
	 * @param appliance
	 * @return quantity in stock, -1 if appliance not found
	 */
	public int getQuantity(Appliance appliance) {
		if (get(appliance) != null) {
			return get(appliance);
		} else {
			return -1;
		}
	}

	/**
	 * search(brand: String, model: String): Appliance
	 * 
	 * Searches inventory for the appliance whose brand and model matches that
	 * passed in as arguments.
	 * 
	 * @param brand - brand of appliance
	 * @param model - model of appliance
	 * @return a reference to the appliance if found, null if not
	 */
	public Appliance search(String brand, String model) {
		Appliance appliance;
		for (Iterator<Appliance> iterator = getItems(); iterator.hasNext();) {
			appliance = iterator.next();
			if (appliance.match(brand, model)) {
				return appliance;
			}
		}
		return null;
	}

	/**
	 * toString(): String
	 * 
	 * @return a description and list of appliances present in inventory
	 */
	public String toString() {
		String applianceList = "Appliance\tBrand\t\tModel\t\t   Price, $     Quantity\n"
				+ "-----------------------------------------------------------------------\n";
		Appliance appliance;
		for (Iterator<Appliance> iterator = getItems(); iterator.hasNext();) {
			appliance = iterator.next();
			applianceList += String.format("%-12s\t%-12s\t%-16s\t%.2f\t\t%d%n", appliance.getClass().getName(),
					appliance.getBrand(), appliance.getModel(), appliance.getPrice(), get(appliance));
		}
		return applianceList;
	}

	/**
	 * listOf(type: int): String
	 * 
	 * Returns a list of appliances in inventory that are of a specific type. The
	 * appliances are displayed according to the format defined in the
	 * ApplianceVisitor passed in.
	 * 
	 * @param type    - an integer that specify the type of appliance
	 * @param visitor - an object that implements the ApplianceVisitor interface
	 *                that defines the format
	 * @return the list of appliances as a String object
	 */
	public String listOf(int type, ApplianceVisitor visitor) {
		String list = visitor.getHeader(type);
		String applianceType = visitor.getType(type);
		Appliance appliance;
		Iterator<Appliance> iterator = getItems();
		while (iterator.hasNext()) {
			appliance = iterator.next();
			if (appliance.getClass().getName().equalsIgnoreCase(applianceType)) {
				list += appliance.accept(visitor) + "\t\t\t" + getQuantity(appliance) + "\n";
			}
		}
		return list;
	}
}