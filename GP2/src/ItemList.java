import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

/**
 * ItemList<T, K> is a generic collection class that stores its objects in a
 * TreeMap. Basic methods are provided for access.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 * @param <T> a key
 * @param <K> a value
 */
public abstract class ItemList<T, K> implements Serializable {
	private static final long serialVersionUID = 1L;
	private Map<T, K> collection;

	/**
	 * Protected constructor for ItemList<T,K>. Instantiates the TreeMap collection.
	 */
	protected ItemList() {
		collection = new TreeMap<T, K>();
	}

	/**
	 * get(key: T): K
	 * 
	 * A getter method for the value of the key.
	 * 
	 * @param key - the key
	 * @return the value
	 */
	public K get(T key) {
		return collection.get(key);
	}

	/**
	 * add(item: T): boolean
	 * 
	 * Adds an item to the collection if it is not already in it.
	 * 
	 * @param item - an item of type T
	 * @return true if addition was successful
	 */
	public boolean add(T item) {
		if (collection.containsKey(item)) {
			return false;
		} else {
			collection.put(item, null);
			return true;
		}
	}

	/**
	 * updateValue(key: T, value: K): boolean
	 * 
	 * Updates the value associated with the key object passed in.
	 * 
	 * @param key   - the key of type T
	 * @param value - the updated value of the key
	 * @return true if the update was successful
	 */
	public boolean updateValue(T key, K value) {
		if (!collection.containsKey(key)) {
			return false;
		}
		collection.put(key, value);
		return true;
	}

	/**
	 * getItems(): Iterator<T>
	 * 
	 * Returns an iterator for the set of keys.
	 * 
	 * @return an iterator object
	 */
	public Iterator<T> getItems() {
		return (Iterator<T>) collection.keySet().iterator();
	}

	/**
	 * getLast(): T
	 * 
	 * Returns the last object of in the set of keys. T must implement comparable to
	 * be organized in a TreeMap.
	 * 
	 * @return the last key
	 */
	public T getLast() {
		try {
			return ((TreeMap<T, K>) collection).lastKey();

		} catch (NoSuchElementException ex) {
			// the map is empty
			return null;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return null;
		}
	}
}
