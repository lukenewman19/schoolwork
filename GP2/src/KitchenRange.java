import java.io.Serializable;

/**
 * KitchenRange class extends Appliance with no additional attributes and an
 * accept method to accept a visitor.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class KitchenRange extends Appliance implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for KitchenRange. Invokes the super constructor.
	 * 
	 * @param brand
	 * @param model
	 * @param price
	 */
	public KitchenRange(String brand, String model, double price) {
		super(brand, model, price);
	}

	/**
	 * accept(visitor: ApplianceVisitor): String
	 * 
	 * KitchenRange accepts an object that implements the ApplianceVisitor
	 * interface. The visitor visits this object by passing it into its visit()
	 * method.
	 * 
	 * @return the String created by the visitor object
	 */
	public String accept(ApplianceVisitor visitor) {
		return visitor.visit(this);
	}

}
