import java.io.Serializable;

/**
 * Refrigerator class extends Appliance with the additional attribute capacity.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class Refrigerator extends Appliance implements Serializable {
	private static final long serialVersionUID = 1L;
	// Liters
	private double capacity;

	/**
	 * Constructor for refrigerator.
	 * 
	 * @param brand
	 * @param model
	 * @param price
	 * @param capacity
	 */
	public Refrigerator(String brand, String model, double price, double capacity) {
		super(brand, model, price);
		this.capacity = capacity;
	}

	/**
	 * Getter method for capacity of the refrigerator.
	 * 
	 * @return the capacity
	 */
	public double getCapacity() {
		return capacity;
	}

	/**
	 * accept(visitor: ApplianceVisitor): String
	 * 
	 * Refrigerator accepts an object that implements the ApplianceVisitor
	 * interface. The visitor visits this object by passing it into its visit()
	 * method.
	 * 
	 * @return the String created by the visitor object
	 */
	public String accept(ApplianceVisitor visitor) {
		return visitor.visit(this);
	}
}
