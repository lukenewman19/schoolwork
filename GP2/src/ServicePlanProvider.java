import java.io.Serializable;

/**
 * ServicePlanProvider interface is a type used by Appliance objects. Some
 * Appliances may allow for repair plan services and must implement this
 * interface.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public interface ServicePlanProvider extends Serializable {

	/**
	 * A getter method for the monthly payment amount.
	 * 
	 * @return the monthly payment amount
	 */
	public double getMonthlyPaymentAmount();

	/**
	 * This method is used to return information about the underlying object. It is
	 * set as returning the brand and model of the appliance in the superclass
	 * Appliance.
	 * 
	 * @return some information about the appliance that allows for service plans
	 */
	public String info();
}
