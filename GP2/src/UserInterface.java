import java.io.File;
import java.util.Scanner;

/**
 * UserInterface serves as a point of access for user to manipulate
 * ApplianceCompany singleton object. Contains one public method to process
 * commands. Private methods work with user commands. UserInterface is also a
 * singleton.
 * 
 * @author Luke Newman, Vadim Malikin, Rasna Kc, Rediet Woldemariam
 *
 */
public class UserInterface {
	private static UserInterface userInterface;
	private ApplianceCompany business;
	private Scanner input = new Scanner(System.in);
	public static final int INVALID_INPUT = -1;
	public static final int EXIT = 0;
	public static final int ADD_CUSTOMER = 1;
	public static final int ADD_APPLIANCE = 2;
	public static final int ADD_TO_INVENTORY = 3;
	public static final int PURCHASE = 4;
	public static final int LIST_CUSTOMERS = 5;
	public static final int LIST_APPLIANCES = 6;
	public static final int DISPLAY_TOTAL_SALES = 7;
	public static final int ENROLL_IN_REPAIR_PLAN = 8;
	public static final int WITHDRAW_FROM_REPAIR_PLAN = 9;
	public static final int BILL_REPAIR_PLAN = 10;
	public static final int LIST_ALL_USERS_REPAIR_PLAN = 11;
	public static final int LIST_ALL_BACKORDERS = 12;
	public static final int SAVE = 13;
	public static final int HELP = 14;

	/**
	 * Private constructor for UserInterface Instantiates UserInterface and prompts
	 * the user for a file. ApplianceCompany singleton is created anew or obtained
	 * from file.
	 */
	private UserInterface() {
		if (promptUser("Load data from disk? (yes or no)").equalsIgnoreCase("yes")) {
			promptForFile();
		} else {
			System.out.println("Not loading from disk. New instance created.");
		}
		business = ApplianceCompany.instance();
	}

	/**
	 * instance(): UserInterface
	 * 
	 * @return the user interface for appliance company
	 */
	public static UserInterface instance() {
		if (userInterface == null) {
			userInterface = new UserInterface();
		}
		return userInterface;
	}

	/**
	 * process(): void
	 * 
	 * The main method that processes user commands. Quits upon exit command.
	 */
	public void process() {
		int command;
		help();
		while ((command = getCommand()) != EXIT) {
			switch (command) {
			case ADD_CUSTOMER:
				addCustomers();
				break;
			case ADD_APPLIANCE:
				addAppliances();
				break;
			case ADD_TO_INVENTORY:
				addToInventory();
				break;
			case PURCHASE:
				makePurchase();
				break;
			case LIST_CUSTOMERS:
				listCustomers();
				break;
			case LIST_APPLIANCES:
				listAppliances();
				break;
			case DISPLAY_TOTAL_SALES:
				displayTotalSales();
				break;
			case ENROLL_IN_REPAIR_PLAN:
				enrollInRepairPlan();
				break;
			case WITHDRAW_FROM_REPAIR_PLAN:
				withdrawFromRepairPlan();
				break;
			case BILL_REPAIR_PLAN:
				billRepairPlan();
				break;
			case LIST_ALL_USERS_REPAIR_PLAN:
				listAllUsersRepairPlan();
				break;
			case LIST_ALL_BACKORDERS:
				listAllBackOrders();
				break;
			case SAVE:
				save();
				break;
			case HELP:
				help();
				break;
			default:
				System.out.println("Not a valid option. Try Again.");
				break;
			}
		}
	}

	/**
	 * promptForFile(): void
	 * 
	 * Obtains filename from user. If the file exists, the WasherCompnay singleton
	 * is taken from file. Calls static method ApplianceCompany.load(String
	 * filename) to obtain the ApplianceCompany on file or if there is already an
	 * instance of ApplianceCompany it will obtain that one and the file will be
	 * ignored. This is to maintain the singleton property of ApplianceCompany.
	 */
	private void promptForFile() {
		String filename = promptUser("What is the file name? provided file is \"gp2\"");
		File file;
		do {
			file = new File(filename);
			if (file.exists()) {
				business = (ApplianceCompany) ApplianceCompany.load(filename);
			} else {
				filename = promptUser("File cannot be found. Enter another file or skip by hitting enter and"
						+ "\na new ApplianceCompany will be created.");

			}
		} while (!file.exists() && !filename.equals(""));
	}

	/**
	 * getCommand(): int
	 * 
	 * Returns the command given by the user.
	 * 
	 * @return int - the command given by the user, -1 if not integer format
	 */
	private int getCommand() {
		try {
			System.out.println("Enter a command: (14 for help)");
			int command = input.nextInt();
			return command;
		} catch (Exception ex) {
			// User types in something else besides an integer
			System.out.println("Only integers 0 - 9 are valid options.");
			input.nextLine();
			return INVALID_INPUT;
		}
	}

	/**
	 * promptUser(prompt: String): String
	 * 
	 * Returns the information from the user asked for in its parameter
	 * 
	 * @param prompt - what is being requested by the system
	 * 
	 * @return String - information provided by user
	 */
	private String promptUser(String prompt) {
		System.out.println(prompt);
		return input.nextLine();
	}

	/**
	 * addCustomers(): void
	 * 
	 * Adds customers to ApplianceCompany. User provides information for customers
	 * to be created one at a time until going back to main menu.
	 */
	private void addCustomers() {
		input.nextLine();
		do {
			String name = promptUser("Please enter the name of customer: ");
			String phoneNumber = promptUser("Please enter the phone number of customer: ");
			if (business.addCustomer(name, phoneNumber)) {
				System.out.println("Customer " + name + " created and stored.");
			} else {
				System.out.println("Failed to add customer.");
			}
		} while (promptUser("Do you want to add another customer? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * addAppliances(): void
	 * 
	 * Adds appliances to ApplianceCompany. User provides information for appliances
	 * to be created one at a time until going back to main menu.
	 */
	private void addAppliances() {
		input.nextLine();
		do {
			String type = promptUser("Please enter the type of appliance:\n"
					+ "1. Dishwasher\t2. Dryer\t3. Furnace\t4. KitchenRange\t5. Refrigerator\t6. Washer\n");
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				double price = Double.parseDouble(promptUser("Please enter the price: "));
				switch (Integer.parseInt(type)) {
				case ApplianceCompany.WASHER:
				case ApplianceCompany.DRYER:
					double monthlyPayment = Double.parseDouble(promptUser("Please enter the monthlyPayment:"));
					if (business.addModel(Integer.parseInt(type), brand, model, price, monthlyPayment, -1.0, -1.0)) {
						System.out.println("Appliance added.");
					} else {
						System.out.println("Unable to add appliance.");
					}
					break;
				case ApplianceCompany.KITCHEN_RANGE:
				case ApplianceCompany.DISHWASHER:
					if (business.addModel(Integer.parseInt(type), brand, model, price, -1.0, -1.0, -1.0)) {
						System.out.println("Appliance added.");
					} else {
						System.out.println("Unable to add appliance.");
					}
					break;
				case ApplianceCompany.FURNACE:
					double maximumHeatingOutput = Double
							.parseDouble(promptUser("Please enter the maximum heating output in BTU:"));
					if (business.addModel(Integer.parseInt(type), brand, model, price, -1.0, -1.0,
							maximumHeatingOutput)) {
						System.out.println("Appliance added.");
					} else {
						System.out.println("Unable to add appliance.");
					}
					break;
				case ApplianceCompany.REFRIGERATOR:
					double capacity = Double.parseDouble(promptUser("Please enter the  capacity in Liters:"));
					if (business.addModel(Integer.parseInt(type), brand, model, price, -1.0, capacity, -1.0)) {
						System.out.println("Appliance added.");
					} else {
						System.out.println("Unable to add appliance.");
					}
					break;
				default:
					System.out.println("Invalid type.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid value for price. Appliance not added.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		} while (promptUser("Do you want to add another appliance? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * addToInventory(): void
	 * 
	 * Adds stock to inventory of ApplianceCompany. Takes in information provided by
	 * user to increase the quantity of a specific appliance. User can continue to
	 * add stock to appliances until ready for another operation. As written new
	 * appliances will not be created if user provided brand and model names that
	 * are not in inventory.
	 */
	private void addToInventory() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				int quantity = Integer.parseInt(promptUser("Please enter the quantity: "));
				int backOrdersFulfilled = business.addToInventory(brand, model, quantity);
				if (backOrdersFulfilled > 0) {
					System.out.println("Stock updated.  " + backOrdersFulfilled + " backorders able to be completed.");
				} else if (backOrdersFulfilled == 0) {
					System.out.println("Stock updated.");
				} else {
					System.out
							.println("Unable to add stock. Invalid input, no match for appliance or invalid quantity.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage() + ex.toString() + ex.getStackTrace());
			}
		} while (promptUser("Do you want to add stock to another appliance? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * makePurchase(): void
	 * 
	 * Attempts to purchase appliances for customers. If there aren't enough
	 * appliances in inventory for a purchase, a full or partial BackOrder is
	 * created. Any partial amount of appliances that can be sold are sold and the
	 * rest is back-ordered.
	 */
	private void makePurchase() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				int customerId = Integer.parseInt(promptUser("Please enter the customer ID: "));
				int quantity = Integer.parseInt(promptUser("Please enter the quantity: "));
				// sold is what we were able to sell
				// business.purchase(...) performs the ApplianceCompany purchase operation
				int sold = business.purchase(brand, model, customerId, quantity);
				if (sold >= 0) {
					if (sold == 0) {
						System.out.println("Out of stock. Placed on backorder.");
					} else if (sold < quantity) {
						System.out.println(sold + " able to be purchased.  The remaining placed on backorder.");
					} else {
						System.out.println("Purchase successful.");
					}
				} else if (sold == INVALID_INPUT) {
					System.out.println("Invalid input.  Unable to process transaction.");
				} else {
					System.out.println("Not enough in stock. Backorder not available for this appliance.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		} while (promptUser("Do you want to make another purchase? yes or no").equalsIgnoreCase("yes"));

	}

	/**
	 * listCustomers(): void
	 * 
	 * Calls listCustomers() method of ApplianceCompany to obtain the list of
	 * customers.
	 */
	private void listCustomers() {
		System.out.println(business.listCustomers());
	}

	/**
	 * listAppliances: void
	 * 
	 * Calls listAppliances() method of ApplianceCompany to obtain the list of
	 * appliances. Passes in the type provided by the user and an ApplianceVisitor
	 * object for defining how it wants the Appliances displayed.
	 */
	private void listAppliances() {
		input.nextLine();
		int type = Integer.parseInt(promptUser("Which type of appliance would you like to list:\n"
				+ "All Appliances: 0\tDishwashers: 1\tDryers: 2\tFurnaces: 3\tKitchenRanges: 4\tRefrigerators: 5\tWashers: 6"));
		System.out.println(business.listOf(type, new ApplianceDisplayListFormat()));
	}

	/**
	 * displayTotalSales(): void
	 *
	 * Calls the displayTotalSales() method of ApplianceCompany to get the total
	 * sales.
	 */
	private void displayTotalSales() {
		System.out.println(business.getTotalSales());
	}

	/**
	 * enrollInRepairPlan(): void
	 * 
	 * Enrolls a customer with a service plan for a valid appliance, if the input
	 * matches a customer and a valid appliance for repair plans.
	 */
	private void enrollInRepairPlan() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				int customerId = Integer.parseInt(promptUser("Please enter the customer ID:"));
				if (business.enrollInRepairPlan(brand, model, customerId)) {
					System.out.println("Successfully enrolled.");
				} else {
					System.out.println(
							"Unable to enroll. Invalid appliance, customer, or customer is already enrolled in that plan.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input for customer id.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		} while (promptUser("Do you want to enroll in another repair plan? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * withdrawFromRepairPlan(): void
	 * 
	 * Withdraws a customer from a repair plan if that customer was previously
	 * enrolled in a plan with the provided appliance.
	 * 
	 */
	private void withdrawFromRepairPlan() {
		input.nextLine();
		do {
			String brand = promptUser("Please enter the brand name: ");
			String model = promptUser("Please enter the model name: ");
			try {
				int customerId = Integer.parseInt(promptUser("Please enter the customer ID:"));
				if (business.withdrawFromRepairPlan(brand, model, customerId)) {
					System.out.println("Successfully withdrawn.");
				} else {
					System.out.println("Unable to withdraw.");
				}
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input for customer id.");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		} while (promptUser("Do you want to withdraw from another repair plan? yes or no").equalsIgnoreCase("yes"));
	}

	/**
	 * billRepairPlan(): void
	 * 
	 * Bills all customers who are enrolled in repair plans and prints the amount
	 * billed.
	 */
	private void billRepairPlan() {
		System.out.println("$" + business.billRepairPlans() + "  in service plans billed.");
	}

	/**
	 * listAllUsersRepairPlan(): void
	 * 
	 * Lists all customers enrolled in repair plans in the format provided by the
	 * method in ApplianceCompany.
	 */
	private void listAllUsersRepairPlan() {
		System.out.println(business.listUsersInRepairPlans());
	}

	/**
	 * listAllBackOrders(): void
	 * 
	 * Lists all BackOrders pending the system in the format provided by the
	 * listAllBackOrders() method and ApplianceDisplayBackOrderFormat.
	 */
	private void listAllBackOrders() {
		System.out.println(business.listAllBackOrders());
	}

	/**
	 * save(): void
	 * 
	 * Receives the filename from the user and then attempts to save that file by
	 * calling the save() method of ApplianceCompany. ApplianceCompany handles its
	 * own saving.
	 */
	private void save() {
		input.nextLine();
		String filename = promptUser("Please enter the filename you want to save to: ");
		if (business.save(filename)) {
			System.out.println("Saved Successfully.");
		} else {
			System.out.println("There was a problem in saving this file.");
		}
	}

	/**
	 * help(): void
	 * 
	 * Prints the command values.
	 */
	private void help() {
		System.out.println("0 - Exit\n1 - Add Customer\n2 - Add Appliance\n"
				+ "3 - Add to Inventory\n4 - Purchase\n5 - List Customers\n"
				+ "6 - List Appliances\n7 - Display Total Sales\n8 - Enroll in a repair plan\n"
				+ "9 - Withdraw from a repair plan\n10 - Bill repair plan\n"
				+ "11 - List all users in repair plan\n12 - List all backorders\n13 - Save\n14 - Help");
	}

	/**
	 * main method instantiates a UserInterface and calls its process() method.
	 */
	public static void main(String[] args) {

		UserInterface.instance().process();

	}
}
