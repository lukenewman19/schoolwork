import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * GUI to implement the VehicleDisplay interface.
 *
 * @author Luke, Vadim, Rediet, Rasna
 */
public class GUIDisplay extends Application implements VehicleDisplay {
	private Button on;
	private Button off;
	private Button park;
	private Button drive;
	private Button accelerator;
	private Button breakButton;
	private Text machineStatus = new Text("Machine Off");
	private Text speedValue = new Text("Current Speed: 0");
	private Text gearStatus = new Text("Gear: Park");
	private Text breakAcceleratStatus = new Text("Break Engaged");
	private static VehicleDisplay display;
	private VehicleContext vehicleContext;

	/**
	 * Returns the instance.
	 * 
	 * @return the object
	 */
	public static VehicleDisplay getInstance() {
		return display;
	}

	/**
	 * Sets up the interface
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		vehicleContext = VehicleContext.instance();
		vehicleContext.setDisplay(this);
		display = this;
		on = new Button(" Turn On");
		off = new Button("Turn Off");
		park = new Button("Park");
		drive = new Button("Drive");
		accelerator = new Button("Accelerator");
		breakButton = new Button("Break");

		GridPane pane = new GridPane();
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setPadding(new Insets(10, 10, 10, 10));
		pane.add(machineStatus, 0, 0);
		pane.add(gearStatus, 1, 0);
		pane.add(speedValue, 2, 0);
		pane.add(breakAcceleratStatus, 3, 0);
		pane.add(on, 4, 0);
		pane.add(off, 5, 0);
		pane.add(park, 6, 0);
		pane.add(drive, 7, 0);
		pane.add(accelerator, 8, 0);
		pane.add(breakButton, 9, 0);
		on.setOnAction((e) -> {
			VehicleContext.instance().onRequested();
		});
		off.setOnAction((e) -> {
			VehicleContext.instance().offRequested();
		});
		park.setOnAction((e) -> {
			VehicleContext.instance().parkRequested();
		});
		drive.setOnAction((e) -> {
			VehicleContext.instance().driveRequested();
		});
		accelerator.setOnAction((e) -> {
			VehicleContext.instance().acceleratorEngaged();
		});
		breakButton.setOnAction((e) -> {
			VehicleContext.instance().breakEngaged();
		});
		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Vehicle");
		primaryStage.setMinWidth(1000);
		try {
			while (vehicleContext == null) {
				Thread.sleep(1000);
			}
		} catch (Exception e) {

		}
		primaryStage.show();
		primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent window) {
				System.exit(0);
			}
		});
	}

	/**
	 * Display the speed of the vehicle
	 */
	@Override
	public void showCurrentSpeed(int speed) {
		speedValue.setText("Current Speed: " + speed);
	}

	/**
	 * Indicate that the machine is on
	 */
	@Override
	public void showMachineOn() {
		machineStatus.setText("Machine On");
	}

	/**
	 * Indicate that the machine is off
	 */
	@Override
	public void showMachineOff() {
		machineStatus.setText("Machine Off");
	}

	/**
	 * display the the gear on drive
	 */
	@Override
	public void showGearDrive() {
		gearStatus.setText("Gear: Drive");
	}

	/**
	 * display the the gear on park
	 */
	@Override
	public void showGearPark() {
		gearStatus.setText("Gear: Park");
	}

	/**
	 * Indicate that the accelerator is engaged
	 */
	@Override
	public void showAcceleratorEngaged() {
		breakAcceleratStatus.setText("Accelerator Engaged");
	}

	/**
	 * Indicate that the break is applied
	 */
	@Override
	public void showBreakEngaged() {
		breakAcceleratStatus.setText("Break Engaged");
	}
}
