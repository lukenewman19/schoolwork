import javafx.application.Application;

/**
 * Starting point for the application.
 * 
 * @author Brahma Dathan and Sarnath Ramnath
 * @Copyright (c) 2010
 */
public class Main {
	public static void main(String[] args) {
		Clock.instance();
		new Thread() {
			@Override
			public void run() {
				Application.launch(GUIDisplay.class, args);
			}
		}.start();
		try {
			while (GUIDisplay.getInstance() == null) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException ie) {
		}
		VehicleDisplay display = GUIDisplay.getInstance();
		VehicleContext.instance().setDisplay(display);
	}
}
