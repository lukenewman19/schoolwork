/**
 * An entity that can be notified of timing events
 * 
 * @author Vadim
 *
 */
public interface Notifiable {
	/**
	 * Process timer ticks
	 */
	public void timerTicked();
}
