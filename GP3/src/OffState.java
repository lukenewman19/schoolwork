/**
 * Represents the vehicle in the off state.
 *
 */
public class OffState extends VehicleState {

	private static OffState instance;

	/**
	 * Private constructor for the singleton pattern
	 */
	private OffState() {
		instance = this;
	}

	/**
	 * For the singleton pattern
	 * 
	 * @return the object
	 */
	public static OffState instance() {
		if (instance == null) {
			instance = new OffState();
		}
		return instance;
	}

	/**
	 * Turns the vehicle on.
	 */
	@Override
	public void onRequested() {
		VehicleContext.instance().changeState(OnParkBreakState.instance());
	}

	/**
	 * initialize the state
	 * 
	 */
	@Override
	public void enter() {
		VehicleContext.instance().showMachineOff();
		// VehicleContext.instance().showCurrentSpeed();
	}

	/**
	 * Leaving the off state means its being turned on.
	 */
	@Override
	public void leave() {
		VehicleContext.instance().showMachineOn();
	}

}
