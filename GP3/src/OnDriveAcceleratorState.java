/**
 * Represents the state of the vehicle in drive with the accelerator engaged.
 * 
 * @author Luke, Vadim, Rediet, Rasna
 *
 */
public class OnDriveAcceleratorState extends VehicleState implements Notifiable {
	private static OnDriveAcceleratorState instance;
	private Timer timer;

	/**
	 * Private constructor for the singleton pattern.
	 */
	private OnDriveAcceleratorState() {
	}

	/**
	 * Returns the instance.
	 * 
	 * @return the object
	 */
	public static OnDriveAcceleratorState instance() {
		if (instance == null) {
			instance = new OnDriveAcceleratorState();
		}
		return instance;
	}

	/**
	 * Method for the event of applying the brakes. Changes state.
	 */
	@Override
	public void breakEngaged() {
		VehicleContext.instance().changeState(OnDriveBreakState.instance());
	}

	/**
	 * Method for performing the action of increasing speed upon notification of the
	 * timer that it ticked.
	 */
	@Override
	public void timerTicked() {
		int newSpeed = currentSpeed + 5;
		if (newSpeed >= 50) {
			currentSpeed = 50;
			timer.stop();
		} else {
			currentSpeed = newSpeed;
		}
		VehicleContext.instance().showCurrentSpeed();
	}

	/**
	 * Initializes the state. Adds itself as a listener to a new timer. Updates the
	 * display.
	 * 
	 */
	@Override
	public void enter() {
		timer = new Timer(this);
		VehicleContext.instance().showAcceleratorEngaged();
		VehicleContext.instance().showCurrentSpeed();
	}

	/**
	 * Leaves state. Throws timer away.
	 */
	@Override
	public void leave() {
		if (timer != null) {
			timer.stop();
			timer = null;
		}
	}
}
