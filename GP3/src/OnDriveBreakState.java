/**
 * Represents the state of the vehicle in drive while the break is applied.
 * 
 * @author Luke, Vadim, Rediet, Rasna
 *
 */
public class OnDriveBreakState extends VehicleState implements Notifiable {
	private static OnDriveBreakState instance;
	private Timer timer;

	/**
	 * Private constructor for the singleton pattern
	 */
	private OnDriveBreakState() {
	}

	/**
	 * Returns the instance.
	 * 
	 * @return the object
	 */
	public static OnDriveBreakState instance() {
		if (instance == null) {
			instance = new OnDriveBreakState();
		}
		return instance;
	}

	/**
	 * Method for the event of engaging the accelerator.
	 */
	@Override
	public void acceleratorEngaged() {
		VehicleContext.instance().changeState(OnDriveAcceleratorState.instance());
	}

	/**
	 * Method for performing the action of decreasing speed upon notification of the
	 * timer that it ticked.
	 */
	@Override
	public void timerTicked() {
		int newSpeed = currentSpeed - 5;
		if (newSpeed <= 0) {
			currentSpeed = 0;
			timer.stop();
		} else {
			currentSpeed = newSpeed;
		}
		VehicleContext.instance().showCurrentSpeed();
	}

	/**
	 * Method for the event of parking.
	 */
	@Override
	public void parkRequested() {
		if (currentSpeed == 0) {
			VehicleContext.instance().changeState(OnParkBreakState.instance());
		}
	}

	/**
	 * Initializes the state. Adds itself as a listener to a new timer. Updates the
	 * display.
	 */
	@Override
	public void enter() {
		timer = new Timer(this);
		VehicleContext.instance().showGearDrive();
		VehicleContext.instance().showBreakEngaged();
		VehicleContext.instance().showCurrentSpeed();
	}

	/**
	 * Leaves state. Throws timer away.
	 */
	@Override
	public void leave() {
		if (timer != null) {
			timer.stop();
			timer = null;
		}
	}
}
