/**
 * Represents the state of the vehicle in park.
 * 
 * @author Luke, Vadim, Rediet, Rasna
 *
 */
public class OnParkBreakState extends VehicleState {
	private static OnParkBreakState instance;

	/**
	 * returns the instance
	 * 
	 * @return this object
	 */
	public static OnParkBreakState instance() {
		if (instance == null) {
			instance = new OnParkBreakState();
		}
		return instance;
	}

	/**
	 * Method for the event of putting the vehicle into drive.
	 */
	@Override
	public void driveRequested() {
		VehicleContext.instance().changeState(OnDriveBreakState.instance());
	}

	/**
	 * Method for the event of turning the vehicle off.
	 */
	@Override
	public void offRequested() {
		VehicleContext.instance().changeState(OffState.instance());
	}

	/**
	 * Initializes state. Updates the display.
	 */
	@Override
	public void enter() {
		VehicleContext.instance().showGearPark();
		VehicleContext.instance().showCurrentSpeed();
	}

}
