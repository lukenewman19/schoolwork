/**
 * The context class is a singleton that maintains the vehicle state and acts as
 * the facade between the GUI and the vehicle state.
 * 
 * @author Luke, Vadim, Rediet, Rasna
 */
public class VehicleContext {
	private VehicleDisplay display;
	private VehicleState currentState;
	private static VehicleContext instance;

	/**
	 * Private constructor for the singleton pattern.
	 */
	private VehicleContext() {
		instance = this;
		currentState = OffState.instance();
	}

	/**
	 * Returns the instance.
	 * 
	 * @return the object
	 */
	public static VehicleContext instance() {
		if (instance == null) {
			instance = new VehicleContext();
		}
		return instance;
	}

	/**
	 * The display could change. So we have to set its reference.
	 * 
	 * @param display The current display object
	 */
	public void setDisplay(VehicleDisplay display) {
		this.display = display;
	}

	/**
	 * 
	 */
	public void initialize() {
		instance.changeState(OffState.instance());
	}

	/**
	 * Called from the states to change the current state
	 * 
	 * @param nextState the next state
	 */
	public void changeState(VehicleState nextState) {
		currentState.leave();
		currentState = nextState;
		currentState.enter();
	}

	/**
	 * Method for the event of turning the vehicle on.
	 */
	public void onRequested() {
		currentState.onRequested();
	}

	/**
	 * Method for the event of turning the vehicle off.
	 */
	public void offRequested() {
		currentState.offRequested();
	}

	/**
	 * Method for the event of parking.
	 */
	public void parkRequested() {
		currentState.parkRequested();
	}

	/**
	 * Method for the event of putting the vehicle into drive.
	 */
	public void driveRequested() {
		currentState.driveRequested();
	}

	/**
	 * Method for the event of engaging the break.
	 */
	public void breakEngaged() {
		currentState.breakEngaged();
	}

	/**
	 * Method for the event of engaging the accelerator.
	 */
	public void acceleratorEngaged() {
		currentState.acceleratorEngaged();
	}

	/**
	 * Updates the display to show the current speed.
	 */
	public void showCurrentSpeed() {
		display.showCurrentSpeed(VehicleState.currentSpeed);
	}

	/**
	 * Updates the display to show the machine is on.
	 */
	public void showMachineOn() {
		display.showMachineOn();
	}

	/**
	 * Updates the display to show the machine is off.
	 */
	public void showMachineOff() {
		display.showMachineOff();
	}

	/**
	 * Updates the display to show the gear is in park.
	 */
	public void showGearPark() {
		display.showGearPark();
	}

	/**
	 * Updates the display to show the gear is in drive.
	 */
	public void showGearDrive() {
		display.showGearDrive();
	}

	/**
	 * Updates the display to show the break is engaged.
	 */
	public void showBreakEngaged() {
		display.showBreakEngaged();
	}

	/**
	 * Updates the display to show the accelerator is engaged.
	 */
	public void showAcceleratorEngaged() {
		display.showAcceleratorEngaged();
	}
}
