/**
 * Specifies what the display system should do. Note that the implementation has
 * a lot of freedom to choose its display.
 */
public interface VehicleDisplay {

	/**
	 * Displays the current speed
	 * 
	 * @param speed
	 *            speed
	 */
	public void showCurrentSpeed(int speed);

	/**
	 * Indicate that the machine is on
	 */
	public void showMachineOn();

	/**
	 * Indicate that the machine is off
	 */
	public void showMachineOff();

	/**
	 * Indicate that the gear is in park position
	 */
	public void showGearPark();

	/**
	 * Indicate that the gear is in drive position
	 */
	public void showGearDrive();

	/**
	 * indicate that the accelerator is engaged
	 */
	public void showAcceleratorEngaged();

	/**
	 * indicate that the break is engaged
	 */
	public void showBreakEngaged();

}
