/**
 * Abstract superclass for the heirarchy of vehicle states.
 * 
 * @author Luke, Vadim, Rediet, Rasna
 */
public abstract class VehicleState {
	protected static int currentSpeed = 0;

	/**
	 * Initializes the state. Default is no action.
	 */
	public void enter() {

	}

	/**
	 * Performs any necessary clean up while leaving the state. Default is no
	 * action.
	 */
	public void leave() {

	}

	/**
	 * Method for the event of turning the vehicle on.
	 */
	public void onRequested() {

	}

	/**
	 * Method for the event of turning the vehicle off.
	 */
	public void offRequested() {

	}

	/**
	 * Method for the event of parking.
	 */
	public void parkRequested() {

	}

	/**
	 * Method for the event of putting the vehicle into drive.
	 */
	public void driveRequested() {

	}

	/**
	 * Method for the event of engaging the break.
	 */
	public void breakEngaged() {

	}

	/**
	 * Method for the event of engaging the accelerator.
	 */
	public void acceleratorEngaged() {

	}

	/**
	 * Method called by
	 */
	public void timerTicked() {

	}
}
