<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Athletes</title>
</head>
<body>
	<c:if test="${!empty errorMessages}">
		<p>
		<c:forEach var="message" items="${errorMessages}">
			${message}<br>
		</c:forEach>
		</p>
	</c:if>
	<form action="Olympics" method="post">
		<table>
			<tr>
				<th>National ID</th>
				<th>Last Name</th>
				<th>First Name</th>
				<th>Date of Birth</th>
				<th>Age</th>
				<th>&nbsp;</th>
			</tr>
			<c:forEach var="athlete" items="${athletes}" >
				<tr>
					<td>${athlete.nationalID}</td>
					<td>${athlete.lastName}</td>
					<td>${athlete.firstName}</td>
					<td>${athlete.dateOfBirth}</td>
					<td>${athlete.age}</td>
					<td>
						<a href="#">Edit</a>
						<a href="#">Delete</a>
					</td>
				</tr>
			</c:forEach>
			<tr>
				<td><input type="submit" name="action" value="Add an Athlete"></td>
			</tr>
		</table>
	</form>
</body>
</html>