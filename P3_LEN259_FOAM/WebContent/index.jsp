<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Athlete</title>
<style>
	table,th,td{
		border: 1px solid black;
	}
	#errorMessages{
		color: red;
	}
</style>
</head>
<body>
	<c:if test="${!empty errorMessages}">
		<ul id="errorMessages">
		<c:forEach var="message" items="${errorMessages}">
			<li>${message}</li>
		</c:forEach>
		</ul>
	</c:if>
	<form action="Olympics" method="post">
		<table>
			<tr>
				<td>National ID</td>
				<td><input type="text" name="nationalID" value="${param.nationalID}"></td>
			</tr>
			<tr>
				<td>First Name</td>
				<td><input type="text" name="firstName" value="${param.firstName}"></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="lastName" value="${param.lastName}"></td>
			</tr>
			<tr>
				<td>Date of Birth</td>
				<td><input type="text" name="dateOfBirth" value="${param.dateOfBirth}"> (mm/dd/yyyy)</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="action" value="Add">
					<input type="submit" name="action" value="Cancel">
				</td>
				
			</tr>
		</table>
	</form>
</body>
</html>