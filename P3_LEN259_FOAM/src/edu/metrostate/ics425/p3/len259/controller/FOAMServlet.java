package edu.metrostate.ics425.p3.len259.controller;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.metrostate.ics425.foam.data.Roster;
import edu.metrostate.ics425.foam.data.RosterException;
import edu.metrostate.ics425.foam.model.Athlete;
import edu.metrostate.ics425.p3.len259.model.AthleteBean;
@WebServlet(name="AthleteBean", urlPatterns = {"/Olympics"})
public class FOAMServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public void init() throws ServletException{
		ServletContext sc = getServletContext();
		String filename = sc.getRealPath("/WEB-INF/demo.roster");
		if (!Roster.initialize(filename)) {
			log("Failed to initialize roster.");
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws IOException, ServletException {
		
		ServletContext sc = getServletContext();
		
		String action = request.getParameter("action");
		String url = "/index.jsp";
		List<String> messages = new ArrayList<String>();
		
		if (action == null) {
			action = "";
		}
		
		if (action.equals("Add")) {
			// 1. Get Data
			String nationalID = request.getParameter("nationalID");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String dateOfBirthString = request.getParameter("dateOfBirth");
			LocalDate dateOfBirth = null;
			// 2. Validate
			if (nationalID == null || nationalID.equals("")) {
				messages.add("National ID is a required field.");
			}
			
			if (firstName == null || firstName.equals("")) {
				messages.add("First Name is a required field.");
			}
			
			if (lastName == null || lastName.equals("")) {
				messages.add("Last Name is a required field.");
			}
			
			if (dateOfBirthString != null && !dateOfBirthString.equals("")) {
				int month;
				int day;
				int year;
				String[] dob = dateOfBirthString.split("/");
				if (dob.length != 3) {
					messages.add("Invalid entry for Date of Birth. Invalid format.");
				} else {
					try{
						month = Integer.parseInt(dob[0]);
						day = Integer.parseInt(dob[1]);
						year = Integer.parseInt(dob[2]);
						dateOfBirth = LocalDate.of(year, month, day);
						log(dateOfBirth.toString());
					}catch (DateTimeException ex) {
						messages.add("Invalid entry for Date of Birth. Day-Month-Year out of range.");
					}catch (NumberFormatException ex) {
						messages.add("Invalid entry for Date of Birth. Expecting integers.");
					}catch (Exception ex) {
						messages.add(ex.getMessage());
					}
				}
			}
			// 3. Process
			List<Athlete> athletes = null;
			if (nationalID != null && firstName != null && lastName != null 
					&& !nationalID.equals("") && !firstName.equals("") && !lastName.equals("")) {
				AthleteBean newAthlete = new AthleteBean(nationalID, firstName, lastName, dateOfBirth);
				try {
					Roster roster = Roster.getInstance();
					if(roster.isOnRoster(nationalID)) {
						messages.add("Athlete with that National ID is already on the roster.");
					} else if(dateOfBirthString.equals("") || dateOfBirth != null) {
						if(dateOfBirth != null) {
							if (dateOfBirth.isAfter(LocalDate.of(1900,1,1))) {
								if(roster.add(newAthlete)) {
									athletes = roster.findAll();
									url = "/displayAthletes.jsp";
								} else {
									messages.add("Failed to add athlete.");
								}
							}else {
								messages.add("Date of Birth must be after January 1st, 1900.");
							}
						}else {
							if(roster.add(newAthlete)) {
								athletes = roster.findAll();
								url = "/displayAthletes.jsp";
							} else {
								messages.add("Failed to add athlete.");
							}
						}
					} 
				} catch (RosterException ex) {
					messages.add("Unable to obtain roster.");
				}
			}
			// 4. Store
			request.setAttribute("athletes", athletes);
			request.setAttribute("errorMessages", messages);
		}
		
		if (action.equals("Cancel")) {
			try {
				// 1. Get
				List<Athlete> athletes = Roster.getInstance().findAll();
				// Skip 2 and 3.
				// 4. Store
				request.setAttribute("athletes", athletes);
			} catch (RosterException ex) {
				messages.add("Unable to obtain roster.");
				request.setAttribute("errorMessages", messages);
			}
			url = "/displayAthletes.jsp";
		}
		// 5. Forward
		sc.getRequestDispatcher(url).forward(request,response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
		throws IOException, ServletException {
		doPost(request, response);
	}
}
