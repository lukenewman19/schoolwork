package edu.metrostate.ics425.p3.len259.model;

import java.io.Serializable;
import java.time.LocalDate;
import edu.metrostate.ics425.foam.model.Athlete;
/**
 * An AthleteBean implements the Interface Athlete to represent an athlete.
 * 
 * @author Luke Newman
 *
 */
public class AthleteBean implements Athlete, Serializable {

	private static final long serialVersionUID = 1L;
	private String nationalID;
	private String firstName;
	private String lastName;
	private LocalDate dateOfBirth;
	private final static int eligibleAge = 16;
	private final static LocalDate OLYMPIC_DATE = LocalDate.of(2020, 7, 24);
	
	/**
	 * No-Arg constructor for AthleteBean.
	 */
	public AthleteBean() {
		
	}
	
	/**
	 * AthleteBean constructor with all of the athlete attributes as arguments. 
	 * 
	 * @param firstName
	 * @param lastName
	 * @param dateOfBirth
	 */
	public AthleteBean(String nationalID, String firstName, String lastName, LocalDate dateOfBirth) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.nationalID = nationalID;
	}
	
	/**
	 * Computes and returns the age of the Athlete.
	 * 
	 * @return the age of the Athlete
	 */
	@Override
	public int getAge() {
		if (dateOfBirth == null) {
			return Athlete.UNSET_DATE;
		} else if (OLYMPIC_DATE.compareTo(dateOfBirth) < 0){
			return Athlete.INVALID_DATE;
		} else {
			return dateOfBirth.until(OLYMPIC_DATE).getYears();
		}
	}
	
	/**
	 * Getter method for date of birth.
	 * 
	 * @return dateOfBirth
	 */
	@Override
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	
	/**
	 * Sets the date of birth for the Athlete.
	 * 
	 * @param dateOfBirth
	 */
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	/**
	 * Getter method for first name of Athlete.
	 * 
	 * @return firstName
	 */
	@Override
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets the first name of the Athlete.
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Getter method for last name of Athlete.
	 * 
	 * @return lastName
	 */
	@Override
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets the last name of the Athlete.
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Getter method for the National ID of the Athlete.
	 * 
	 * @return nationalID
	 */
	@Override
	public String getNationalID() {
		return nationalID;
	}
	
	/**
	 * Sets the national ID of the Athlete.
	 * 
	 * @param nationalID
	 */
	public void setNationalID(String nationalID) {
		this.nationalID = nationalID;
	}
	/**
	 * Computes the eligibility of the Athlete.
	 * 
	 * @return boolean - eligible or not
	 */
	@Override
	public boolean isEligible() {
		if (dateOfBirth == null) {
			return false;
		} else {
			return (dateOfBirth.until(OLYMPIC_DATE).getYears() >= eligibleAge);
		}
	}
	
	/**
	 * Overrides the toString() method in the Object class.  
	 * 
	 * @return a description of the Athlete
	 */
	@Override
	public String toString() {
		return String.format("Name: %s %s NationalID: %s Date of Birth: %tF", firstName, lastName, nationalID, dateOfBirth);
	}

}
